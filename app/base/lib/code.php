<?php

/**
 * Created by PhpStorm.
 * User: aiden-pc
 * Date: 2014/11/13
 * Time: 10:31
 * 自定义错误码集合类
 */

/**
 * 状态码长度：5位数字字符串
 * 格式：**(模块/业务代号) + ***(递增)
 * 00***：为系统错误码
 * 注：状态码不能出现重复
 *
 * Class base_code
 */
class base_code
{

    /**
     * 系统错误码：00***
     */
    private static $_systemCode = array(

        // 系统错误码：00***;
        '00000' => '操作成功！',
        '00001' => '系统繁忙，请稍候再试！',
        '00002' => '操作失败！',
        '00003' => '非法请求！',
        '00004' => '验签失败！',
        '00005' => '参数传递不全，请核实后再提交！',
        '00006' => '参数传递错误，请核实后再提交！',
        '00007' => '参数值非法，请核实后再提交！',
        '00008' => '用户未登录，请登录！',
    );

    /*
     * 业务错误码,错误码值大于00000
     */
    private static $_workCode = array(
		'10000'=>'返回数据为空',
        // UMS错误码：10***
        '10001' => '用户不存在！',
        '10002' => '用户杉德宝已开通！',
    	'10003' => '账号信息不正确',
    	'10004' => '该帐号被禁用',
    		'10005' => '原密码错误',
    		'10006'=>'该账号已经被使用',
    		'10007'=>'用户名不能为空',
    		
    	//商户错误信息
    	'21001' => '门店号不正确',

        // 核销错误码：11***
        '11001' => '该商品已卖光或已下架！',
    	'11002'=>'核销码错误',
    	'11003'=>'请输入核销码',
    	'11004'=>'核销码被使用',

        // 活动错误码：99***
        '99001' => '该活动已参加！',
//        '99002' => '该活动已超过限定人数！',
        '99002' => '你来晚啦！红包已抢完！',

    );

    /**
     * 获取code对应的提示信息
     * @param int $code 错误码
     * @return type
     */
    public static function getMsg($code)
    {
        //$i = intval($code);
        $i = '' . $code;

        return ($code > 10000) ? self::$_workCode[$i] : self::$_systemCode[$i];
    }

    /**
     * 获取所有状态码
     * @return array
     */
    public static function getAllMsg()
    {
        $data = array();
        $data['system'] = self::$_systemCode;
        $data['work'] = self::$_workCode;

        return $data;
    }


    /**
     * 接口统一返回JSON数据
     * @param $code 状态码
     * @param array $data 数据
     * @param string $msg 提示信息
     */
    public function _print($code, $data = array(), $msg = '')
    {
        if (0 === strlen($msg)) {
            // 获取code对应的提示信息
            $msg = $this->getMsg($code);
        }

        $res = array(
            'code' => $code,
            'msg' => $msg ? $msg : '',
            'data' => $data ? $data : array()
        );

        $res = json_encode($res);
		kernel::log("返回值：". $res, '', 'api.php');
        header("Content-type: application/json");
        //echo $res;

        // JSONP
        $res = ($_GET && isset($_GET['callback'])) ? $_GET['callback'] . '(' . $res . ')' : $res;

        echo $res;
        exit;
    }

}
