<?php

/**
 * Created by PhpStorm.
 * User: aiden-pc
 * Date: 2015/1/12
 * Time: 14:25
 */
class sysapi_route
{
	private $method_list = array();		//请求白名单
	
    public function __construct(&$app)
    {
//         $this->method_list = include_once(app::get('sysapi')->app_dir . "/config.php");
// 		if(!$this->method_list){
// 			kernel::single("base_code")->_print('00003', '', '非法请求，不存在白名单');
// 		}
    }

    /**
     * 接口请求单入口
     * 请求地址示例：
     * http://localhost/sand/index.php/openapi/sysapi_route/accept?method=init.init.wcEcoupon&v=1
     */
    function  accept()
    {
    	sleep(1);
        $params = $_REQUEST;

        //拒绝非法请求
//         if(!array_key_exists($params['method'], $this->method_list)){
//         	kernel::single('base_code')->_print('00003', '', '非法请求');
//         }
        //kernel::log('请求参数：' . json_encode($params), '', 'api.php');
        $paramStr = http_build_query($params);
        kernel::log('静态页ajax请求：' . $paramStr, '', 'api.php');

        /**
         * 解析	应用模块.类名.方法名
         * 组合格式：	类名	sysapi_api_init_init
         * 			方法	wcEcoupon_v1
         * 执行：类名->方法
         */
        $methodArray = explode('.', $params['method']);
        $v = $params['v'];  // 接口版本
        $class = 'sysapi_api_' . $methodArray[0] . '_' . $methodArray[1];
        $method = $methodArray[2] . '_' . $v;

        // Clas 是否存在
        $isClass = false;
        try {
            if (class_exists($class)) {
                $isClass = true;
            }
        } catch (Exception $e) {
        }

        if (!$isClass) {
            kernel::single("base_code")->_print('00003');
        }

        // 方法是否存在
        if (!method_exists(kernel::single($class), $method)) {
            kernel::single("base_code")->_print('00003');
        }

        $res = kernel::single($class)->$method($params);

        $code = isset($res['code']) ? $res['code'] : '';
        $data = isset($res['data']) ? $res['data'] : array();
        $msg = isset($res['msg']) ? $res['msg'] : '';

        /**
         * 接口统一返回JSON数据
         * @param $code 状态码
         * @param array $data 数据
         * @param string $msg 提示信息
         */
        kernel::single("base_code")->_print($code, $data, $msg);
    }
}