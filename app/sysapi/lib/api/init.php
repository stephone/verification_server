<?php

/**
 * 各终端首页专用接口(各终端专用)
 * Created by PhpStorm.
 * User: aiden-pc
 * Date: 2015/1/12
 * Time: 17:18
 */
class sysapi_api_init
{
    /**
     * 微信核销首页接口
     * User: aiden-pc
     * Date: 2015/1/12
     * @param $params
     * @return array
     */
    public function wcEcoupon_1($params)
    {
        $cashcouponList = kernel::single('sysapi_ecoupon_cashcoupon')->getList($_REQUEST);
        if (empty($cashcouponList['list'])) {
            // todo 无数据
        } else {
            // 处理列表信息
            $cashcouponList['list'] = kernel::single('sysapi_ecoupon_cashcoupon')->dealList($cashcouponList['list']);
        }

        $data = array(
//            // 核销商品列表
//            'cashcouponList' => $cashcouponList,
//            'cartNumber' => kernel::single('sysapi_ecoupon_cart')->getCartNumber(), // 购物车数量
            'member' => app::get('b2c')->model('members')->get_current_member(),    // 用户信息
            'catList' => kernel::single('ecoupon_cashcoupon')->getList(),           // 核销分类列表
            'regionList' => kernel::single('ecoupon_cashcoupon')->getRegionList(),  // 核销地区列表
        );

        return array('code' => '00000', 'data' => $data);
    }

}