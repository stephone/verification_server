<?php
/**
 * 会员相关接口
 * @author ym.liu
 *
 */
class sysapi_api_verification_members{
	
	//登录接口
	public function login_1($params){
		$type = trim($params['type']);
		$username = trim($params['username']);
		$password = md5(trim($params['password']));
		$num = trim($params['num']);
		$info = kernel::single('verification_members')->login($username, $password, $type, $num);

		if($info['status']){
			kernel::single('base_code')->_print('00000', $info['data'], '登录成功'); 
		} else {
			kernel::single('base_code')->_print($info['code']);
		}
	}
	
	//重置密码接口
	public function reset_1($params){
		$member_id = trim($params['member_id']);
		$password = trim($params['oldpassword']);
		$newPassword = trim($params['newpassword']);
		$sid = trim($params['sid']);
		if($params['newpassword'] != $params['repassword']){
			kernel::single('base_code')->_print('00002', '', '新密码两次输入不一致');
		}

		if(empty($password) || empty($newPassword)){
			kernel::single('base_code')->_print('00005', '', '密码不能为空');
		}
		
		$info = kernel::single('verification_members')->resetPssword($member_id, $password, $newPassword, $sid);
		kernel::single('base_code')->_print($info['code']);
	}
	
	//退出登录接口
	public function logOut_1($params){
		$member_id = trim($params['member_id']);
		$sid = trim($params['sid']);
		$res = kernel::single('verification_members')->logOut($member_id, $sid);
		kernel::single('base_code')->_print($res['code']);
	}
}