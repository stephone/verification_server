<?php
/**
 * 核销接口
 * @author ym.liu
 *
 */
class sysapi_api_verification_verification{
	
	/**
	 * 核销码核销接口
	 * @param array $params 参数数组
	 */
	public function verify_1($params){
		$vRes = kernel::single('verification_mdl_verification')->validate($params);
		if(!$vRes['status']){
			kernel::single('base_code')->_print($vRes['code'], '', $vRes['msg']);
		}

		$res = kernel::single('verification_verification')->verify($params);
		if($res['status']){
			/**
			 * {"code":"00000","msg":"\u6838\u9500\u6210\u529f","data":{"time":"2015-03-13 06:43:31","code":"K324324","store_id":"22"}}
			 */
			kernel::single('base_code')->_print($res['code'], $res['data'], '核销成功');
		} else {
			kernel::single('base_code')->_print($res['code'], '', $res['msg']);
		}
	}
}
