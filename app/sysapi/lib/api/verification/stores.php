<?php
class sysapi_api_verification_stores{

	//获取门店信息
	public function getStoreInfo_1($params){
		$sid = $params['sid'];
		$store_num = trim($params['store_num']);
		$print = kernel::single('base_code');
		if(empty($store_num)){
			$print->_print('00005', '', '门店号不能为空');
		}
		$res = kernel::single('verification_stores')->getStoreInfo($store_num, $sid);
		if($res['status']){
			$print->_print('00000', $res['data']);
		} else {
			$print->_print($res['code']);
		}
	}

	/**
	 * 获取某个商户下的所有门店
	 * @param  [type] $params [description]
	 * @return [type]         [description]
	 */
	public function getStoreList_1($params){
		$sid = $params['sid'];
		$sTime = $params['sTime'];
		$eTime = $params['eTime'];
		$page = $params['page'];
		$keyword = htmlspecialchars($params['keyword']);
		$row = $params['row'] ? $params['row'] : 10;
		$res = kernel::single('verification_stores')->getStoreList($sid, $sTime, $eTime, $page, $keyword, $row);

		if($res['status']){
			kernel::single('base_code')->_print('00000', $res['data']);
		} else {
			kernel::single('base_code')->_print($res['code']);
		}
	}
}