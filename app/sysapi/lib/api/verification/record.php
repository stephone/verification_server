<?php
class sysapi_api_verification_record{
	
	/**
	* 通过门店ID，查询条件获取核销记录
	*/
	public function getRecord_1($params){
		$store_num = trim($params['store_num']);
		$seller_id = trim($params['seller_id']);
		$sTime = strtotime($params['sTime'] . ' 00:00:00'); 
		$eTime = strtotime($params['eTime'] . ' 23:59:59');
		$page = intval($params['page']);
		$row = intval($params['row']) ? intval($params['row']) : 10;
		$count = isset($params['count']) ? isset($params['count']) : 'false';

		$vRes = kernel::single('verification_record')->getRecord($store_num,$seller_id, $sTime, $eTime,$count, $page, $row);
		if($vRes['status']){
			kernel::single('base_code')->_print('00000', $vRes['data'], ''); 
		} else {
			kernel::single('base_code')->_print($vRes['code'], '', ''); 
		}
	}

	/**
	 * 通过record_id获取核销信息
	 * @param  array $params record_id号
	 */
	public function getRecordById_1($params){
		$record_id = $params['record_id'];
		$res = kernel::single('verification_record')->getRecordInfo($record_id);
		if($res['status']){
			kernel::single('base_code')->_print('00000', $res['data']);
		} else {
			kernel::single('base_code')->_print($res['code']);
		}
	}
}