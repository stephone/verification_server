<?php

/**
 * Created by PhpStorm.
 * User: zou.gl
 * Date: 14-7-15
 * Time: 上午10:53
 */
class desktop_finder_queue
{
    var $column_tools = '操作';

    var $column_tools_width = '50';

    function column_tools($row)
    {
        return '<a href="index.php?app=desktop&ctl=queue&act=redoView&queue_id=' . $row['queue_id'] . '" target="dialog::{ title:\'' . app::get('b2c')->_('重新发送') . '\', width:600}">' . app::get('b2c')->_('重试') . '</a>';
        return $returnValue;

    }

    var $detail_edit = '详细列表';

    function detail_edit($id)
    {
        $render = app::get('desktop')->render();

        $oItem = kernel::single("base_mdl_queue");
        $items = $oItem->getList('*', array('queue_id' => $id), 0, 1);
        $render->pagedata['item'] = $items[0];

//        $data = $items[0]['params']['data'];
//        $render->pagedata['params'] = $this->arrayToString($items[0]['params']);

        $res = array();
        $this->format_result($items[0]['params'], $res);
        $render->pagedata['params'] = $res;

        $render->display('queue/detail.html');

    }

    /**
     * 多维数组转字符串
     * @param $arr
     * @return string
     */
    function arrayToString($arr)
    {
        if (is_array($arr)) {
            return implode('<br />', array_map('arrayToString', $arr));
        }
        return $arr;
    }


    /**
     * 格式化请求参数
     * @param $data
     * @param array $res
     */
    private function format_result($data, & $res = array())
    {
        if (is_array($data)) {
            $this->format_array($data, $res);
        } else {
            $res = $this->addBR($data);
        }
    }

    /**
     * 格式化数组
     * @param $data
     * @param array $res
     * @param string $kPrefix
     */
    private function format_array($data, & $res = array(), $kPrefix = '')
    {
        foreach ($data as $k => $v) {
            if (is_array($v)) {
                $this->format_array($v, $res, $k);
            } else {

                $k = $kPrefix ? $kPrefix . '[' . $k . ']' : $k;

                // 临时方案：过滤掉 &nbsp; 组合字符串
                if (count(explode('&nbsp;', $v)) > 1) {
                    $res[$k] = $v;
                    continue;
                }

                $arr = explode('&', $v);
                if (count($arr) > 1) {
                    foreach ($arr as $param) {
                        $par = explode('=', $param);
                        //$res[$par[0]] = $par[1];
                        $res["{$k}[{$par[0]}]"] = $this->addBR($par[1]);
                    }
                } else {

                    $res[$k] = $this->addBR($v);
                }
            }
        }
    }


    /**
     * 超长字符串添加<br />
     * @param $str
     * @return string
     */
    private function addBR($str)
    {
        $res = '';
        $arrStr = $this->str_split_unicode($str, 100);
        foreach ($arrStr as $s) {
            $res .= $s . '<br />';
        }
        return $res;
    }

    /**
     * 将unicode字符串按传入长度分割成数组
     * @param  string $str 传入字符串
     * @param  integer $l 字符串长度
     * @return mixed      数组或false
     */
    function str_split_unicode($str, $l = 0)
    {
        if ($l > 0) {
            $ret = array();
            $len = mb_strlen($str, "UTF-8");
            for ($i = 0; $i < $len; $i += $l) {
                $ret[] = mb_substr($str, $i, $l, "UTF-8");
            }
            return $ret;
        }
        return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
    }

}