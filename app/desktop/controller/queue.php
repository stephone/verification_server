<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */
 
class desktop_ctl_queue extends desktop_controller{

    var $workground = 'desktop_ctl_system';

    function index(){
        $params = array(
            'title'=>app::get('desktop')->_('队列管理'),
            'actions'=>array(
                array('label'=>app::get('desktop')->_('全部启动'),'submit'=>'index.php?app=desktop&ctl=queue&act=run'),
                array('label'=>app::get('desktop')->_('全部暂停'),'submit'=>'index.php?app=desktop&ctl=queue&act=pause'),
                ),
            );
        $this->finder('base_mdl_queue',$params);
    }

    function run(){
        $this->begin('index.php?app=desktop&ctl=queue&act=index');
        $queue_model = app::get('base')->model('queue');
        foreach((array)$_POST['queue_id'] as $id){
            $item['queue_id'] = $id;
            $item['status'] = 'hibernate';
            $queue_model->save($item);        
        }
        $queue_model->flush();
        $this->end(true,app::get('desktop')->_('启动成功'));
    }
    
    function pause(){
        $this->begin('index.php?app=desktop&ctl=queue&act=index');
        $queue_model = app::get('base')->model('queue');
        foreach((array)$_POST['queue_id'] as $id){
            $item['queue_id'] = $id;
            $item['status'] = 'paused';
            $queue_model->save($item);
        }
        $this->end(true,app::get('desktop')->_('暂停成功'));
    }

    function redoView() {
        $queueId = intval($_GET['queue_id']);
        $this->pagedata['queue_id'] = $queueId;
        $this->page('admin/queue.html');
    }
    /**
     * 重新发送队列
     */
    function redoSend()
    {
        $queueId = intval($_POST['queue_id']);

        $queueModel = app::get('base')->model('queue');
        $queueModel->db->exec('update sdb_base_queue set status="hibernate" where queue_id=' . $queueId);
        // JSON
        $res = $queueModel->runtaskOne($queueId);
        echo json_encode($res);
        exit;
    }

}
