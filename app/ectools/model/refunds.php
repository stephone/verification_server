<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */
 

class ectools_mdl_refunds extends dbeav_model{
    
    var $has_many = array(
        'orders'=>'order_bills@ectools:contrast:refund_id^bill_id',
    );

    var $defaultOrder = array('t_begin','DESC');

    function gen_id(){
        $i = rand(0,9999);
        do{
            if(9999==$i){
                $i=0;
            }
            $i++;
            $refund_id = time().str_pad($i,4,'0',STR_PAD_LEFT);
            $row = $this->dump($refund_id, 'refund_id');
        }while($row);
        return $refund_id;
    }

    /**
     * 模板统一保存的方法
     * @params array - 需要保存的支付信息
     * @params boolean - 是否需要强制保存
     * @return boolean - 保存的成功与否的进程
     */
    public function save($data,$mustUpdate = null)
    {
        // 异常处理    
        if (!isset($data) || !$data || !is_array($data))
        {
            trigger_error(app::get('ectools')->_("支付单信息不能为空！"), E_USER_ERROR);exit;
        }
        
        $sdf = array();
       
        // 支付数据列表
        $background = true;//后台 todo

        $payment_data = $data;
        $sdf_payment = parent::dump($data['refund_id'],'*');

        if ($sdf_payment) 
        {
            if($sdf_payment['status'] == $data['status']
                || ($sdf_payment['status'] != 'progress' && $sdf_payment['status'] != 'ready')){
                return true;
            }    
            if($data['currency'] && $sdf_payment['currency'] != $data['currency']){
                return false;
            }
        }

        if($sdf_payment){
            $sdf = array_merge($sdf_payment, $data);
        }else{
            $sdf = $data;
            //$sdf['status'] = 'ready';
        }
        // 保存支付信息（可能是退款信息）
        $is_succ = parent::save($sdf);
        
        return $is_succ;
    }
	
	/**
     * 得到所有的支付账号
     * @param null
     * @return null
     */
    public function getAccount()
    {
        $query = 'SELECT DISTINCT bank, account FROM ' . $this->table_name(1) .' WHERE status="succ"';
        return $this->db->select($query);
    }
    
    /**
     * 重写搜索的下拉选项方法
     * @param null
     * @return null
     */
    public function searchOptions(){
        $columns = array();
        foreach($this->_columns() as $k=>$v){
            if(isset($v['searchtype']) && $v['searchtype']){
                $columns[$k] = $v['label'];
            }
        }
        
        // 添加额外的
        $ext_columns = array('rel_id'=>$this->app->_('订单号'));
        
        return array_merge($columns, $ext_columns);
    }
	
	public function _filter($filter,$tableAlias=null,$baseWhere=null){
		if(!$filter)
			return parent::_filter($filter);

		if (array_key_exists('rel_id', $filter))
		{
			$obj_order_bills = $this->app->model('order_bills');
			$bill_filter = array(
				'rel_id|has'=>$filter['rel_id'],
				'bill_type'=>'refunds',
			);
			$row_order_bills = $obj_order_bills->getList('bill_id',$bill_filter);
			$arr_member_id = array();
			if ($row_order_bills)
			{
				$arr_order_bills = array();
				foreach ($row_order_bills as $arr)
				{
					$arr_order_bills[] = $arr['bill_id'];
				}
				$filter['refund_id|in'] = $arr_order_bills;				
			}
			else
			{
				$filter['refund_id'] = 'a';
			}
			unset($filter['rel_id']);
		}

        $filter = parent::_filter($filter);
        return $filter;
    }
       
    /**
     * delete 方法重载
     *
     * 根据条件删除条目
     * 不可以由pipe控制
     * 可以广播事件
     * 
     * @param mixed $filter 
     * @param mixed $named_action 
     * @access public
     * @return void
     */
    public function delete($filter)
    {
        return parent::delete($filter);
    }
	
	/**
     * filter字段显示修改
     * @params string 字段的值
     * @return string 修改后的字段的值
     */
    public function modifier_member_id($row)
    {
        if (is_null($row) || empty($row))
        {
            return app::get('ectools')->_('未知会员或非会员');
        }
        $arr_pam_account = app::get('b2c')->model('members')->getList('login_name', array('member_id' => $row));
		if ($arr_pam_account[0])
			return $arr_pam_account[0]['login_name'];
		else
			return '-';
    }
	
	/**
     * filter字段显示修改
     * @params string 字段的值
     * @return string 修改后的字段的值
     */
    public function modifier_op_id($row)
    {
        if (is_null($row) || empty($row))
        {
            return app::get('ectools')->_('未知操作员');
        }
        $arr_pam_account = app::get('b2c')->model('members')->getList('login_name', array('member_id' => $row));
		if ($arr_pam_account[0])
			return $arr_pam_account[0]['login_name'];
		else
			return app::get('ectools')->_('未知操作员');
    }
	
	/**
     * filter字段显示修改
     * @params string 字段的值
     * @return string 修改后的字段的值
     */
    public function modifier_pay_app_id($row)
    {
        $obj_payment_cfgs = $this->app->model('payment_cfgs');
		$arr_payment_cfgs = $obj_payment_cfgs->getPaymentInfo($row);
		
		if ($arr_payment_cfgs)
		{
			return $arr_payment_cfgs['app_name'];
		}
		else
			return 'app_name';
    }
	
	/** 
	 * 退款货币值
	 */
	public function modifier_cur_money($row)
    {
        $currency = $this->app->model('currency');
		$filter = array('refund_id' => $this->pkvalue);
        $tmp = $this->getList('currency', $filter);	
		$arr_cur = $currency->getcur($tmp[0]['currency']);
		$row = $currency->formatNumber($row,false,false);
		
		return $arr_cur['cur_sign'] . $row;
    }
	
	/**
	 * 退款收款人帐号
	 */
	public function modifier_account($row)
	{
		if (is_null($row) || empty($row))
        {
            return app::get('ectools')->_('未知收款人');
        }
		
		return $row;
	}

    public function getOrderLogList($refund_id, $page=0, $limit=-1)
    {
        $objlog = $this->app->model('refund_log');
        $arrlogs = array();
        $arr_returns = array();

        if ($limit < 0)
        {
            $arrlogs = $objlog->getList('*', array('rel_id' => $refund_id));
        }

        $limitStart = $page * $limit;

        $arrlogs_all = $objlog->getList('*', array('rel_id' => $refund_id));
        $arrlogs = $objlog->getList('*', array('rel_id' => $refund_id), $limitStart, $limit);
        if ($arrlogs)
        {
            foreach ($arrlogs as &$logitems)
            {
                switch ($logitems['behavior'])
                {
                    case 'creates':
                        $logitems['behavior'] = app::get('b2c')->_("创建");
                        if ($arr_log_text = unserialize($logitems['log_text']))
                        {
                            $logitems['log_text'] = '';
                            foreach ($arr_log_text as $arr_log)
                            {
                                $logitems['log_text'] .= app::get('b2c')->_($arr_log['txt_key']);
                            }
                        }
                        break;
                    case 'verify':
                        $logitems['behavior'] = app::get('b2c')->_("审核");
                        if ($arr_log_text = unserialize($logitems['log_text']))
                        {
                            $logitems['log_text'] = '';
                            foreach ($arr_log_text as $arr_log)
                            {
                                $logitems['log_text'] .= app::get('b2c')->_($arr_log['txt_key']);
                            }
                        }
                        break;
                    case 'payments':
                        $logitems['behavior'] = app::get('b2c')->_("支付");
                        if ($arr_log_text = unserialize($logitems['log_text']))
                        {
                            $logitems['log_text'] = '';
                            foreach ($arr_log_text as $arr_log)
                            {
                                $logitems['log_text'] .= app::get('b2c')->_($arr_log['txt_key'],$arr_log['data'][0],$arr_log['data'][1],$arr_log['data'][2]);
                            }
                        }
                        break;
                    case 'refunds':
                        $logitems['behavior'] = app::get('b2c')->_("退款");
                        if ($arr_log_text = unserialize($logitems['log_text']))
                        {
                            $logitems['log_text'] = '';
                            foreach ($arr_log_text as $arr_log)
                            {
                                $logitems['log_text'] .= app::get('b2c')->_($arr_log['txt_key']);
                            }
                        }
                        break;
                    case 'finish':
                        $logitems['behavior'] =  app::get('b2c')->_("完成");
                        if ($arr_log_text = unserialize($logitems['log_text']))
                        {
                            $logitems['log_text'] = '';
                            foreach ($arr_log_text as $arr_log)
                            {
                                $logitems['log_text'] .= app::get('b2c')->_($arr_log['txt_key']);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        $arr_returns['page'] = count($arrlogs_all);
        $arr_returns['data'] = $arrlogs;

        return $arr_returns;
    }



	/**
     * 重写订单导出方法
     * @param array $data
     * @param array $filter
     * @param int $offset
     * @param int $exportType
     */
    public function fgetlist_csv( &$data,$filter,$offset,$exportType =1 ){
        $limit = 100;
        $cols = $this->_columns();
        if(!$data['title']){
            $this->title = array();
            foreach( $this->getTitle($cols) as $titlek => $aTitle ){
                $this->title[$titlek] = $aTitle;
            }
            // service for add title when export
            foreach( kernel::servicelist('export_add_title') as $services ) {
                if ( is_object($services) ) {
                    if ( method_exists($services, 'addTitle') ) {
                        $services->addTitle($this->title);
                    }
                }
            }
            $data['title'] = '"'.implode('","',$this->title).'"';
        }

        if(!$list = $this->getList(implode(',',array_keys($cols)),$filter,$offset*$limit,$limit))return false;
        
        //$data['contents'] = array();
        foreach( $list as $line => $row ){
            // service for add data when export
            foreach( kernel::servicelist('export_add_data') as $services ) {
                if ( is_object($services) ) {
                    if ( method_exists($services, 'addData') ) {
                        $services->addData($row);
                    }
                }
            }
            $rowVal = array();
            $order_id = app::get('ectools')->model('order_bills')->getList('rel_id',array('bill_id'=>$row['refund_id']));
            $row['order_id'] = $order_id[0]['rel_id']."\r";
			$row['refund_id'] = $row['refund_id']."\r";//科学技术法
			$row['account'] = $row['account']."\r";
			$row['seller_bn'] = $row['seller_bn']."\r";
            //seller_id，order_id
            $seller_name = app::get('seller')->model('sellers')->getList('seller_name',array('seller_id'=>$row['seller_id']));
            $row['seller_id'] = $seller_name[0]['seller_name'];

            foreach( $row as $col => $val ){
                
                if( in_array( $cols[$col]['type'],array('time','last_modify') ) && $val ){
                   $val = date('Y-m-d H:i',$val);
                }
                if ($cols[$col]['type'] == 'longtext'){
                    if (strpos($val, "\n") !== false){
                        $val = str_replace("\n", " ", $val);
                    }
                }
                
                if( strpos( (string)$cols[$col]['type'], 'table:')===0 ){
                    $subobj = explode( '@',substr($cols[$col]['type'],6) );
                    if( !$subobj[1] )
                        $subobj[1] = $this->app->app_id;
                    $subobj = &app::get($subobj[1])->model( $subobj[0] );
                    $subVal = $subobj->dump( array( $subobj->schema['idColumn']=> $val ),$subobj->schema['textColumn'] );
                    $val = $subVal[$subobj->schema['textColumn']]?$subVal[$subobj->schema['textColumn']]:$val;
                }

                if( array_key_exists( $col, $this->title ) )
                    $rowVal[] = addslashes(  (is_array($cols[$col]['type'])?$cols[$col]['type'][$val]:$val ) );
            }
            $contet = '"'.implode('","',$rowVal).'"';
            $data['contents'][] = '"'.$row['order_id'].'",'.$contet;
        }
        $data['title'] = '"订单号",'.$data['title'];
        return true;

    }

	function getTitle(&$cols){
        $title = array();
        foreach( $cols as $col => $val ){
            if( !$val['deny_export'] )
            $title[$col] = $val['label'];
        }
        return $title;
    }
}
