<?php
/**
 * 对members数据表进行操作
 * @author ym.liu
 *
 */
class verification_mdl_members extends dbeav_model {
	
	// 会员表数据检查
	public function validate($data, $add = true) {
		if(empty($data['username'])){
			return array('status'=>false, 'code'=>'10007', 'message'=>'用户名不能为空');
		}
	
		// 查询是否已存在用户
		if ($add) {
			$count = app::get ( 'verification' )->model ( 'members' )->count ( array (
					'username' => $data ['username'] 
			) );
			if ($count > 0) {
				return array('status'=>false, 'code'=>'10006', 'message'=>'该帐号已经使用');
			}

			//用户名检验
			if(!preg_match('/\w{5,15}/', $data['username'])){
				return array('status'=>false, 'message'=>'帐号必须为5到15位的数字或字符串');
			}

			//密码检验
			if(!preg_match('/\w{5,15}/', $data['password'])){
				return array('status'=>false, 'message'=>'密码必须为5到15位的数字或字符串');
			}
		}
		
		return array('status'=>true);
	}
	
	// 通过用户名，密码，登录类型，编号，获取用户信息
	public function getMemberInfo($username, $password, $type, $num) {
		$db = kernel::database();
		$ssql = 'select m.member_id,m.username,m.password,m.type,m.status,m.seller_id,m.store_id,ss.shortname as seller_name,ss.seller_id,s.store_name,s.store_num
                from sdb_verification_members m
                left join sdb_verification_sellers ss on m.seller_id = ss.seller_id
                left join sdb_verification_stores s on m.store_id = s.store_id
                where m.username = '.$db->quote($username).' and m.type = '.$type = $db->quote($type);
        
		$memberInfo = $db->select($ssql);

		if($memberInfo[0]['status'] == 'false'){
			return array('status'=>false, 'code'=>'10004');
		}

		//获取用户信息
		if($type == 'store'){
			if($memberInfo[0]['store_num'] != $num){
				return array('status'=>false, 'code'=>'10003');
			}
		}

		if($memberInfo[0]['password'] == $password && $memberInfo[0]['username'] == $username){
			
			//将获取的密码再次进行md5加密，存储在客户端
			$memberInfo[0]['password'] = md5($password);
			return array('status'=>true, 'data'=>$memberInfo[0]);
		} else {
			return array('status'=>false, 'code'=>'10003');
		}
	}

	//验证用户是否登录
	public function checkLogin($sid){
		session_id($sid);
		session_start();
		//$member_id = $_SESSION['member_id'];
		$seller_id = $_SESSION['seller_id'];
		if(empty($sid)){
			return array('status'=>false, 'code'=>'00008');
		} else {
			return array('status'=>true);
		}
	}
}