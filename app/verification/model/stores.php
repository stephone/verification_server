<?php
class verification_mdl_stores extends dbeav_model{

    public $recycle_msg ;

    public function validate($data){
        $info['status'] = false;
        //门店编号验证
        if(!preg_match('/\d{8}/', $data['store_num'])){
            return $info['message'] = '门店号必须为8位数字';
        }

        //门店名验证
        if(empty($data['store_name'])){
            return $info['message'] = '门店名不能为空';
        }
        $info['status'] = true;
        return $info;
    }

    public function pre_recycle($rows){
        foreach($rows as $v){
            $countMember = $this->app->model('members')->count(array('store_id'=>$v['store_id']));
            if($countMember > 0){
                $this->recycle_msg = '删除出错：请确认已经删除该门店下所有账户。';
                return false;
            }

        }
        return true;
    } 

	function getTitle(&$cols){
        $title = array();
        foreach( $cols as $col => $val ){
            if( !$val['deny_export'] )
            $title[$col] = $val['label'].'('.$col.')';
        }
        return $title;
    }
	
	public function fgetlist_csv( &$data,$filter,$offset,$exportType =1 ){
        $limit = 100;
        $cols = $this->_columns();
        if(!$data['title']){
            $this->title = array();
            foreach( $this->getTitle($cols) as $titlek => $aTitle ){
                $this->title[$titlek] = $aTitle;
            }
            // service for add title when export
            foreach( kernel::servicelist('export_add_title') as $services ) {
                if ( is_object($services) ) {
                    if ( method_exists($services, 'addTitle') ) {
                        $services->addTitle($this->title);
                    }
                }
            }
            $data['title'] = '"'.implode('","',$this->title).'"';
        }

        if(!$list = $this->getList('*',$filter,$offset*$limit,$limit))return false;
        
        foreach( $list as $line => $row ){
            // service for add data when export
            foreach( kernel::servicelist('export_add_data') as $services ) {
                if ( is_object($services) ) {
                    if ( method_exists($services, 'addData') ) {
                        $services->addData($row);
                    }
                }
            }
            $rowVal = array();

			$row['store_num'] = $row['store_num']."\r";
            foreach( $row as $col => $val ){
                
                if( in_array( $cols[$col]['type'],array('time','last_time') ) && $val ){
                   $val = date('Y-m-d H:i',$val);
                }

                if( array_key_exists( $col, $this->title ) )
                    $rowVal[] = addslashes(  (is_array($cols[$col]['type'])?$cols[$col]['type'][$val]:$val ) );
            }
            $data['contents'][] = '"'.implode('","',$rowVal).'"';
        }
        return true;

    }
}