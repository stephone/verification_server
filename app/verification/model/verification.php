<?php
/**
 * 核销数据表model
 * @author ym.liu
 *
 */
class verification_mdl_verification{
	
	/**
	 * 对传入数据进行验证
	 * @param array $params 待验证的数组参数
	 * @return string 返回信息码
	 */
	public function validate($params){
		
		//核销码非空验证
		if(empty($params['code'])){
			return array('status'=>false, 'code'=>'11003');
		}

		//其他参数验证
		if( empty($params['store_num']) || empty($params['seller_id']) || empty($params['seller_name']) || empty($params['store_name'])){
			return array('status'=>false, 'code'=>'00006');
		}

		//核销码唯一验证
		$count = app::get('verification')->model('record')->count(array('code'=>$params['code']));
		if($count > 0){
			return array('status'=>false, 'code'=>'11004');
		}
		return array('status'=>true);
	}
}
