<?php
/**
 * 核销业务处理，必须在用户登录情况下操作
 * @author ym.liu
 *
 */
class verification_verification{
	
	//核销方法
	public function verify($params){

		$loginRes = kernel::single('verification_mdl_members')->checkLogin($params['sid']);
		if(!$loginRes['status']){
			return $loginRes;
		}

		$vParams = array(
			"terminalNumber" =>$params['store_num'], 	//终端号(必填)，等同于门店号
			"tenantNumer" => $params['seller_id'], 	//商户号
			"terminalNo" => substr($params['store_num'], 2),	//终端流水号，取门店号后6位
			"veriId" =>$params['code'],	//类型值 如手机号或卡号 可以为空
		);


		//$res = kernel::single('verification_api_verification')->register($vParams);
		
		$json = '{"responseCode":"600000","resultInfo":{"adv":"","bizType":"02","cards":"001001","msg":"01/01|,,|","orderAmt":"000000000100","orderId":"E20150320144077","payableAmt":"000000000000","paymentAmt":"000000000100","printType":"0","productCode":"","regId":"20150313000018278845"},"retMag":"成功"}';
		$res = json_decode($json, true);

		//核销第一步
		if($res['responseCode'] == '600000'){
			$payment = intval($res['resultInfo']['paymentAmt']) / 100;
			$data = array(
				'store_num'=>$params['store_num'],
				'member_id'=>$_SESSION['member_id'],
				'username'=>$_SESSION['username'],
				'trading_time'=>time(),
				'code'=>$params['code'],
				'order_id'=>$res['resultInfo']['orderId'],
				'payment'=>number_format($payment, 3),
				'seller_id'=>$params['seller_id'],
				'seller_name'=>$params['seller_name'],
				'store_name'=>$params['store_name']
			);

			//添加核销进本地数据表,如果记录存在需要先删除记录，再次核销
			$rowId = app::get('verification')->model('record')->insert($data);

			if(!$rowId){
				return array('code'=>'11004');
			}
			
			//核销第二步，发送通知
			$vParams['regId'] = $res['resultInfo']['regId'];
			$vParams['paymentAmt'] = $res['resultInfo']['paymentAmt'];
			$noticeRes = $this->sendNotice($vParams);
			
			//开启事务
			$db = kernel::database();
			$transaction_status = $db->beginTransaction(); //开启事务
			$update = app::get('verification')->model('record')->update(array('status'=>'complete'), array('record_id'=>$rowId));
			if($noticeRes['responseCode'] == '600000'){
				$db->commit($transaction_status);
			} else {
				$db->rollBack();
			}
			if($update){
				//更新核销商品信息，增加起止时间，图片等...
				$this->updateGoods($rowId, $data['order_id']);
				
				//返回给静态页使用
				return array('status'=>true, 'code'=>'00000', 'data'=>array(
						'trading_time'=>date('Y-m-d H:i:s', $data['trading_time']),
						'code'=>$data['code'],'record_id'=>$rowId,
						'store_num'=>$data['store_num']
				));
			} else {
				return array('code'=>'00001');
			}
			
		} elseif($res['responseCode'] == '620013'){
			return array('status'=>false,'code'=>'11004');
		}else {
			return array('status'=>false, 'code'=>'11002');
		}
	}
	
	/**
	 * 发送核销通知
	 * @param string $regId 	核销业务注册ID
	 * @param string $paymentAmt 核销金额
	 */
	public function sendNotice($params){
		
		//发送核销通知
		//return kernel::single('verification_api_verification')->payNotice($params);
		 $json = '{"responseCode":"600000","retMag":"成功"}';
		 return json_decode($json, true);
	}
	
	//更新核销商品信息
	public function updateGoods($rowId, $order_id){

		/*
		返回以下json数据被json_decode()后的数组
		{"code":"00000","msg":"\u64cd\u4f5c\u6210\u529f\uff01","data":{"name":"\u5c0f\u7c73","start_time":"1387382400","end_time":"1451553000","url":"public\/images\/9c\/9b\/6a\/d37a99fc7755fded96b6311ab565c5ef.jpg"}}
		*/
	// $str = '{"code":"00000","msg":"\u64cd\u4f5c\u6210\u529f\uff01","data":{"name":"\u5c0f\u7c73","start_time":"1387382400","end_time":"1451553000","url":"public\/images\/9c\/9b\/6a\/d37a99fc7755fded96b6311ab565c5ef.jpg"}}';
	// $res = json_decode($str, true);
		$res = kernel::single('verification_api_verification')->getGoodsInfo($order_id);
		$data = array(
			'name'=>$res['data']['name'],
			'start_time'=>$res['data']['start_time'],
			'end_time'=>$res['data']['end_time'],
			'image_url'=>DOMAIN . $res['data']['url'],
			'sync'=>'ok'
		);
		if($res['code'] == '00000'){
			app::get('verification')->model('record')->update($data, array('record_id'=>$rowId));
		} else {
			kernel::log($order_id . '获取核销商品信息失败', '', 'verification.php');
		}
	}

/**
 * 析构方法
 */
	private function __destruct(){
		//销毁session
		session_destroy();
	}
}