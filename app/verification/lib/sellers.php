<?php
class verification_sellers{

	/**
	 * 获取商户信息
	 * @param  int $store_id 门店ID号
	 * @return array           返回商户信息
	 */
	public function getSellerInfo($store_id){
		$store = app::get('verification')->model('stores')->getList('seller_id', array('store_id'=>$store_id));
		$seller = app::get('verification')->model('sellers')->getList('seller_name, seller_id', array('seller_id'=>$store[0]['seller_id']));
		return $seller[0];
	}
}