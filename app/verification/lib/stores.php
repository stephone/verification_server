<?php
class verification_stores{

	/**
	 * 获取门店列表及对应的核销记录与金额
	 * @param  int $seller_id 商户ID
	 * @param  int $sTime     开始时间
	 * @param  int $eTime     结束时间
	 * @param  int $page      分页数
	 * @param  string $keyword   关键字
	 * @param  int $row       每页记录数
	 * @return array           结果集
	 */
	public function getStoreList($sid, $sTime, $eTime, $page, $keyword, $row){
		
		$loginRes = kernel::single('verification_mdl_members')->checkLogin($sid);
		if(!$loginRes['status']){
			return $loginRes;
		}

		$db = kernel::database();
		$sTime = strtotime($sTime . ' 00:00:00');
		$eTime = strtotime($eTime . ' 23:59:59');
		$limit = ($page - 1) * $row . ',' . $row;
		$sql = sprintf('select store_num,store_name,images as image_url from sdb_verification_stores where seller_id = %s and store_name like "%%%s%%" order by sort desc limit %s', $_SESSION['seller_id'], $keyword, $limit);
		$store = $db->select($sql);

		if($store){
			foreach($store as $k => $v){
				$row = sprintf("select count(*) as count, sum(payment) as sum from sdb_verification_record where store_num = %s and (trading_time between %s and %s)", $v['store_num'], $sTime, $eTime);
				$data = $db->select($row);
				$store[$k]['image_url'] = $v['image_url'];
				$store[$k]['count'] = empty($data[0]['count']) ? 0 : $data[0]['count'] ;
				$store[$k]['sum'] = number_format($data[0]['sum']);
			}
		}

		if(count($store) > 0){
			return array('status'=>true, 'data'=>$store);
		} else {
			return array('status'=>false, 'code'=>'10000');
		}
	}

/**
 * 查询门店信息
 * @param  string $store_num 用户输入的门店号
 * @param  string $sid session信息
 * @return array            结果集
 */
	public function getStoreInfo($store_num, $sid){

		session_id($sid);
		session_start();

		$loginRes = kernel::single('verification_mdl_members')->checkLogin($sid);
		if(!$loginRes['status']){
			return $loginRes;
		}
		
		$filter = array(
			'seller_id'=>$_SESSION['seller_id'],
			'store_num'=>$store_num
			);
		$res = app::get('verification')->model('stores')->getList('store_num, store_name', $filter);
		$store = $res[0];
		if(empty($store)){
			return array('status'=>false,'code'=>'10000');
		} else {
			return array('status'=>true,'code'=>'00000', 'data'=>$store);
		}
	}

/**
 * 析构方法
 */
	private function __destruct(){
		//销毁session
		session_destroy();
	}
}