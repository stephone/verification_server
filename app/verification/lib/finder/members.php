<?php
class verification_finder_members{
	
	var $column_edit = '编辑';
	var $column_sellerName = '商户名称';
	var $column_sellerNum = '商户号';
	var $column_storeName = '门店名称';
	var $column_storeNum = '门店号';

	function column_edit($row){
		return '<a href="index.php?app=verification&ctl=admin_members&act=editMember&_finder[finder_id]='.$_GET['_finder']['finder_id'].'&member_id='.$row['member_id'].'"  target="dialog::{frameable:true, title:\'编辑用户信息\', width:460, height:300}">编辑</a>';
	}

	public function column_sellerName($row){
		$sql = sprintf('select s.seller_name from sdb_verification_members m left join sdb_verification_sellers s on m.seller_id = s.seller_id where m.member_id = %s', $row['member_id']);
		$result = kernel::database()->select($sql);
      	return $result[0]['seller_name'];
	}

	public function column_sellerNum($row){
		$sql = sprintf('select s.seller_id from sdb_verification_members m left join sdb_verification_sellers s on m.seller_id = s.seller_id where m.member_id = %s', $row['member_id']);
		$result = kernel::database()->select($sql);
      	return $result[0]['seller_id'];
	}

	public function column_storeName($row){
		$sql = sprintf('select s.store_name from sdb_verification_members m left join sdb_verification_stores s on m.store_id = s.store_id where m.member_id = %s', $row['member_id']);
		$result = kernel::database()->select($sql);
      	return $result[0]['store_name'];
	}

	public function column_storeNum($row){
		$sql = sprintf('select s.store_num from sdb_verification_members m left join sdb_verification_stores s on m.store_id = s.store_id where m.member_id = %s', $row['member_id']);
		$result = kernel::database()->select($sql);
      	return $result[0]['store_num'];
	}
}