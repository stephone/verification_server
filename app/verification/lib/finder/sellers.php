<?php
class verification_finder_sellers{
	public $detail_stores = '门店列表';
	public $column_edit = '操作';
	public $detail_members = "已开通帐号列表";
	
	public $pagelimit = 5;
	
	public function __construct($app)
	{
		$this->app = $app;
		$this->controller = app::get('verification')->controller('admin_sellers');
	}
	
	//实现编辑方法
	public function column_edit($row){
		$render = $this->app->render();
		$arr = array(
            'app'=>$_GET['app'],
            'ctl'=>$_GET['ctl'],
            'act'=>$_GET['act'],
            'finder_id'=>$_GET['_finder']['finder_id'],
            'action'=>'detail',
            'finder_name'=>$_GET['_finder']['finder_id'],
		);

		$arr_link = array(
            'editSeller'=>array(
                'detail_public'=>array(
                    'href'=>'index.php?app=verification&ctl=admin_sellers&act=edit&seller_id='.$row['seller_id'].'&_finder[finder_id]='.$_GET['_finder']['finder_id'].'&finder_id='.$_GET['_finder']['finder_id'],'label'=>'编辑商户信息',
                    'target'=>"dialog::{ title:'编辑商户信息', width:500, height:230}"
                ),
            ),
            'addStore'=>array(
            	'detail_public'=>array(
                    'href'=>'index.php?app=verification&ctl=admin_stores&act=addStore&seller_id='.$row['seller_id'].'&_finder[finder_id]='.$_GET['_finder']['finder_id'].'&finder_id='.$_GET['_finder']['finder_id'],'label'=>'添加门店',
                    'target'=>"dialog::{ title:'添加门店', width:700, height:500}"
                ),
            ),
            'addMember'=>array(
            	'detail_public'=>array(
            		'href'=>'index.php?app=verification&ctl=admin_members&act=addMember&seller_id='.$row['seller_id'].'&_finder[finder_id]='.$_GET['_finder']['finder_id'].'&finder_id='.$_GET['_finder']['finder_id'],'label'=>'添加商户管理帐号',
            		'target'=>"dialog::{ title:'添加商户管理帐号', width:500, height:250}"
            		),
            	),
            'finder'=>array(
                'detail_remark'=>array(
					'href'=>'javascript:void(0);',
                    'submit'=>'index.php?'.utils::http_build_query($arr).'&finderview=detail_stores&id='.$row['seller_id'].'&_finder[finder_id]='.$_GET['_finder']['finder_id'],'label'=>'下属门店',
                    'target'=>'tab',
		        ),
			),
			'members'=>array(
                'detail_remark'=>array(
					'href'=>'javascript:void(0);',
                    'submit'=>'index.php?'.utils::http_build_query($arr).'&finderview=detail_members&id='.$row['seller_id'].'&_finder[finder_id]='.$_GET['_finder']['finder_id'],'label'=>'查看该商户下所有帐号',
                    'target'=>'tab',
		        ),
			),
		);

		$site_get_policy_method = $this->app->getConf('site.get_policy.method');
		if ($site_get_policy_method == '1')
		{
			unset($arr_link['finder']['detail_point']);
		}

		$render->pagedata['arr_link'] = $arr_link;
		$render->pagedata['handle_title'] = '操作';
		$render->pagedata['is_active'] = 'true';
		return $render->fetch('admin/actions.html');
	}
	
	//添加控制面板查看列表
	function detail_stores($seller_id){
		if(!$seller_id) return null;
		$nPage = $_GET['detail_sellers'] ? $_GET['detail_sellers'] : 1;
		$app = app::get('verification');
		$stores =  $app->model('stores');
		//取得商户号
		$items = $stores->getList('*', array('seller_id' => $seller_id),$this->pagelimit*($nPage-1),$this->pagelimit, 'sort DESC');
		
		$count = $stores->count(array('seller_id'=>$seller_id));

		$render->pagedata['item'] = $items;

		
		$render = $app->render();
		$render->pagedata['item'] = $items;
		if($_GET['page']) unset($_GET['page']);
		$_GET['page'] = 'detail_stores';
		kernel::single('verification_ctl_admin_stores')->pagination($nPage,$count,$_GET);
		return $render->fetch('admin/sellers/finder/detail.html');
	}

	//商户下所有帐号列表
	public function detail_members($seller_id){
		return kernel::single('verification_ctl_admin_members')->detail_members($seller_id);
	}
}