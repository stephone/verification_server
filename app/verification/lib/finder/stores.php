<?php
class verification_finder_stores{
	
	var $column_edit = '操作';
	var $column_pic = '门店缩略图';
	var $column_sellerName = "商户名称";
	public $detail_members = "查看门店帐号列表";

	//缩略图
    function column_pic($row){
        $stores =  app::get('verification')->model('stores')->getList('images', array('store_id'=>$row['store_id']));
		$img_src = $stores[0]['images'];
		if(!$img_src)return '';
		return "<a href='$img_src' class='img-tip pointer' target='_blank'
		        onmouseover='bindFinderColTip(event);'>
		<span>&nbsp;pic</span></a>";
	}

	public function __construct($app)
	{
		$this->app = $app;
		$this->controller = app::get('verification')->controller('admin_stores');
	}

	//虚拟列显示商户名称
	public function column_sellerName($row){
		$seller = kernel::single('verification_sellers')->getSellerInfo($row['store_id']);
		return $seller['seller_name'];
	}

		//实现编辑方法
	public function column_edit($row){
		//通过store_num获取本行所有信息
		$rows = $this->app->model('stores')->getList('*', array('store_num'=>$row['store_num']));
		$row = $rows[0];
		$render = $this->app->render();
		$arr = array(
            'app'=>$_GET['app'],
            'ctl'=>$_GET['ctl'],
            'act'=>$_GET['act'],
            'finder_id'=>$_GET['_finder']['finder_id'],
            'action'=>'detail',
            'finder_name'=>$_GET['_finder']['finder_id'],
		);

		$arr_link = array(


            'editSeller'=>array(
                'detail_public'=>array(
                    'href'=>'index.php?app=verification&ctl=admin_stores&act=editStore&store_id='.$row['store_id'].'&_finder[finder_id]='.$_GET['_finder']['finder_id'].'&finder_id='.$_GET['_finder']['finder_id'],'label'=>'编辑门店信息',
                    'target'=>"dialog::{ title:'编辑门店信息', width:700, height:500}"
                ),
            ),
            'addMember'=>array(
	        	'detail_public'=>array(
	        		'href'=>'index.php?app=verification&ctl=admin_members&act=addMember&seller_id='.$row['seller_id'].'&store_id='.$row['store_id'].'&_finder[finder_id]='.$_GET['_finder']['finder_id'].'&finder_id='.$_GET['_finder']['finder_id'],'label'=>'添加门店管理帐号',
	        		'target'=>"dialog::{ title:'添加门店管理帐号', width:700, height:250}"
	        	),
			),
			'members'=>array(
                'detail_remark'=>array(
					'href'=>'javascript:void(0);',
                    'submit'=>'index.php?'.utils::http_build_query($arr).'&finderview=detail_members&id='.$row['store_id'].'&_finder[finder_id]='.$_GET['_finder']['finder_id'],'label'=>'查看该门店下所有帐号',
                    'target'=>'tab',
		        ),
			),
		);

		$site_get_policy_method = $this->app->getConf('site.get_policy.method');
		if ($site_get_policy_method == '1')
		{
			unset($arr_link['finder']['detail_point']);
		}

		$render->pagedata['arr_link'] = $arr_link;
		$render->pagedata['handle_title'] = '操作';
		$render->pagedata['is_active'] = 'true';
		return $render->fetch('admin/actions.html');
	}

	//查看门店下所有帐号
	public function detail_members($store_id){
		return kernel::single('verification_ctl_admin_members')->detail_storemembers($store_id);
	}
}