<?php
class verification_record{

	/*获取核销记录列表
	* @params string $store_num 终端号
	* @params int $sTime 开始时间
	* @params int $eTime 结束时间
	* @params int $page 第几页
	*/
	public function getRecord($store_num, $seller_id, $sTime, $eTime, $count = 'false', $page = 1, $row = 10){
		if(empty($store_num)){
			return array('status'=>false, 'code'=>'00008');
		}
		$limit = ($page-1) * $row . ',' . $row;
		$sql = sprintf('select record_id,name,order_id,start_time,end_time,payment,image_url,status,trading_time from sdb_verification_record where store_num="%s" and seller_id="%s" and status="complete" and trading_time between %s and %s order by record_id desc limit ' . $limit, $store_num,$seller_id, $sTime, $eTime);
		$result = kernel::database()->select($sql);

		//是否需要统计
		if($count == 'true'){
			$sSql = sprintf('select sum(payment) as sum, count(*) as count from sdb_verification_record where store_num="%s" and seller_id="%s" and status="complete" and trading_time between %s and %s', $store_num,$seller_id, $sTime, $eTime);
			$sum = kernel::database()->select($sSql);
			$data['count'] = $sum[0]['count'];
			$data['sum'] = number_format($sum[0]['sum']);
		}
		

		if(empty($result)){
			return array('status'=>false, 'code'=>'10000');
		} else {
			foreach($result as $k=>$v){
				$v['start_time'] = !empty($v['start_time']) ? date('Y-m-d', $v['start_time']) : '0000-00-00';
				$v['end_time'] = !empty($v['end_time']) ? date('Y-m-d', $v['end_time']) : '0000-00-00';
				$v['payment'] = number_format($v['payment']);
				$v['status'] = $v['status'] == 'complete' ? '已核销' : '核销失败';
				$v['trading_time'] = date('Y-m-d H:i:s', $v['trading_time']);
				$data['list'][] = $v;
			}
			return array('status'=>true, 'data'=>$data);
		}
	}

	/**
	 * 通过record_id获取核销记录信息
	 * @param  int $record_id 核销记录id
	 * @return array 	包含状态码的结果集
	 */
	public function getRecordInfo($record_id){
		$record = app::get('verification')->model('record')->getList('code,sum,store_name,name,status,payment,start_time,end_time,trading_time', array('record_id'=>$record_id));
		$data = $record[0];
		if(empty($data)){
			return array('status'=>false, 'code'=>'10000');
		} else {
			$data['trading_time'] = date('Y-m-d H:i:s', $data['trading_time']);
			$data['start_time'] = !empty($data['start_time']) ? date('Y-m-d', $data['start_time']) : '';
			$data['end_time'] = !empty($data['end_time']) ? date('Y-m-d', $data['end_time']) : '';
			$data['payment'] = number_format($data['payment']);
			return array('status'=>true, 'data'=>$data);
		}
	}
}