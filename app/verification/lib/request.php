<?php
class verification_request{
	
	public function sendJson($params, $url){
		if(empty($params))
		{
			return array(
					'status'=>false,
					'msg'=>'参数不全'
			);
		}
		$json = json_encode($params);
		$res = $this->send($json, $url);
		$resData = json_decode($res, true);
		return $resData;
	}

	/**
	 * 发送&形式参数的请求串
	 * @param  array $params	请求的字符串
	 * @param  array $ur 	请求的url地址
	 * @return array
	 */
	public function sendParms($params, $url){
		$queryStr = http_build_query($params);
		$res = $this->send($queryStr, $url);
		$resData = json_decode($res, true);
		return $resData;
	}

	/**
	 * 发送远程请求
	 * @param  string $params 请求参数
	 * @param  string $url    请求的url地址
	 * @return string         返回获取的值
	 */
	public function send($params, $url){
		kernel::log('后台请求地址：'.$url, '', 'verification.php');
		kernel::log('后台请求参数: '.$params,'',"verification.php");
		$http = new base_httpclient();
		$res = $http->set_timeout('60')->post($url , $params);
		kernel::log('后台接收 : '.$res,'',"verification.php");
		return $res;
	}
}