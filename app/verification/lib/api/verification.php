<?php
/**
 * 核销提供接口，数据获取
 * @author ym.liu
 *
 */
class verification_api_verification{
	
	//核销业务注册
	public function register($params){
		$data = array(
			'apiName'=>'Register',
			'regInfo'=>array(
				"channelFlag" => "O2O",	//核销渠道标识  0:熊猫打折核销，1：久彰O2O
				"terminalNumber" =>$params['terminalNumber'], 	//终端号(必填)
				"tenantNumer" => $params['tenantNumer'], 	//商户号
				"terminalNo" => $params['terminalNo'],	//终端流水号
				"veriType" =>"1", 	//认证类型 0：手机号，1：卡号
				"veriId" =>$params['veriId'],	//类型值 如手机号或卡号 可以为空
				"msgCode" =>"0",	//验证码(必填)
				"entrustingParty" =>"100001",	//委托方 100001久彰
				"flag" =>"0", 	//是否提货类型 0：否，1是
				"version" =>"1016" 	//pos 版本号
			)
		);
		return kernel::single('verification_request')->sendJson($data, REGISTER);
	}
	
	//核销业务通知
	public function payNotice($params){
		$data = array(
				'apiName'=>'PayNotice',
				'regInfo'=>array(
						"channelFlag" => "O2O",	//核销渠道标识  0:熊猫打折核销，1：久彰O2O
						"terminalNumber" =>$params['terminalNumber'], 	//终端号(必填)
						"tenantNumer" => $params['tenantNumer'], 	//商户号
						"terminalNo" => $params['terminalNo'],	//终端流水号
						"regId" =>$params['regId'], 	//认证类型 0：手机号，1：卡号
						"entrustingParty" =>"100001",	//委托方 100001久彰
						"transAmt"=>$params['paymentAmt'],		//交易金额
						"transSerialNo"=>""		//扣款流水号
				)
		);
		return kernel::single('verification_request')->sendJson($data, NOTICE);
	}
	
	/**
	 * 获取商品信息
	* 通过订单ID号，查询，sdb_ecoupon_order_items 获取 cashcoupon_id 核销商品ID号
	* 通过ID查询表sdb_ecoupon_cashcoupon，核销商品的详细信息
	* 请求参数：method=verification.cashcoupon.getDetail&v=1&horder_id=E20150320144077
	*/
	public function getGoodsInfo($order_id){
		$data = array(
			'method'=>'verification.cashcoupon.getDetail',
			'v'=>1,
			'order_id'=>$order_id
		);
		return kernel::single('verification_request')->sendParms($data, SHOPMALL_API_URL);
	}
}