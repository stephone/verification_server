<?php
class verification_ctl_admin_stores extends desktop_controller{
	private $pagelimit = 5;

	// 门店后台首页
	public function index() {
		$this->finder ( 'verification_mdl_stores', array (
				'title' => '门店列表',
				'orderBy' => 'sort desc',
				'allow_detail_popup' => true,
				'use_buildin_import' => true,
				'use_buildin_export' => true,
				'use_buildin_filter'=>true,
				
		) );
	}
	
	//添加门店
	public function addStore(){
		if($_POST){

			$data = array(
					'seller_id'=>I('post.seller_id'),
					'store_num'=>I('post.store_num'),
					'store_name'=>I('post.store_name'),
					'address'=>I('post.address'),
					'tel'=>I('post.tel'),
					'images'=>I('post.images'),
					'province'=>I('post.province'),
					'city'=>I('post.city'),
					'area'=>I('post.area'),
					'longitude'=>I('post.longitude'),
					'latitude'=>I('post.latitude'),
					'create_time'=>time()
			);
			$this->begin();

			$res = kernel::single('verification_mdl_stores')->validate($data);
			if(!$res['status']){
				$this->end(false, $res['message']);
			}

			//检查门店是否存在 
			$count = $this->app->model('stores')->count(array('store_num'=>$data['store_num']));
			if($count > 0){
				$this->end(false, "门店编号已经存在");
			}
			$res = $this->app->model('stores')->insert($data);
			
			if($res){
				$this->end($res, '门店添加成功！');
			} else {
				$this->end($res, '门店添加失败');
			}
		} else {
			$seller_id = I('get.seller_id');
			//获取商户信息
			$seller = $this->app->model('sellers')->getList('seller_name,seller_id', array('seller_id'=>$seller_id));
			$this->pagedata['seller'] = $seller[0];
			$this->display('admin/stores/add.html');
		}
	}
	
	//编辑门店
	public function editStore(){
		if($_POST){
			$data = array(

					'store_id'=>I('post.store_id'),
					'store_num'=>I('post.store_num'),
					'store_name'=>I('post.store_name'),
					'address'=>I('post.address'),
					'tel'=>I('post.tel'),
					'images'=>I('post.images'),
					'province'=>I('post.province'),
					'city'=>I('post.city'),
					'area'=>I('post.area'),
					'longitude'=>I('post.longitude'),
					'latitude'=>I('post.latitude'),
					'last_time'=>time()
			);
			$this->begin();
			$res = kernel::single('verification_mdl_stores')->validate($data);
			if(!$res['status']){
				$this->end(false, $res['message']);
			}

			$res = $this->app->model('stores')->update($data, array('store_id'=>I('post.store_id')));

			if($res){
				$this->end($res, '修改成功');
			} else {
				$this->end($res, '修改失败');
			}
		} else {
			$store_id = I('get.store_id');
			$store = $this->app->model('stores')->getList('*', array('store_id'=>$store_id));

			//获取所属商户信息
			$seller = $this->app->model('sellers')->getList('seller_name', array('seller_id'=>$store[0]['seller_id']));

			$this->pagedata['store'] = $store[0];
			$this->pagedata['seller'] = $seller[0];
			$this->display('admin/stores/add.html');
		}
	}

	//门店列表分页查看
	function detail_stores($id){
		if(!$id) return null;
		$nPage = $_GET['detail_stores'] ? $_GET['detail_stores'] : 1;
		$app = app::get('verification');
		$stores =  $app->model('stores');
		$items = $stores->getList('*', array('seller_id' => $id),$this->pagelimit*($nPage-1),$this->pagelimit,'sort DESC');

		$count = $stores->count(array('seller_id'=>$id));

		$render->pagedata['item'] = $items;

		$render = $app->render();
		$render->pagedata['item'] = $items;
		if($_GET['page']) unset($_GET['page']);
		$_GET['page'] = 'detail_stores';
		$this->pagination($nPage,$count,$_GET);
		return $render->fetch('admin/sellers/finder/detail.html');
	}

	//分页
	public function pagination($current,$count,$get){ //本控制器公共分页函数
		$app = app::get('verification');
		$render = $app->render();
		$ui = new base_component_ui($this->app);
		//unset($get['singlepage']);
		$link = 'index.php?app=verification&ctl=admin_stores&act=ajax_html&id='.$get['id'].'&finder_act='.$get['page'].'&'.$get['page'].'=%d';
		$this->pagedata['pager'] = $ui->pager(array(
				'current'=>$current,
				'total'=>ceil($count/5),
				'link' =>$link,
		));
	}
	
	public function ajax_html()
	{
		$finder_act = $_GET['finder_act'];
		$html = $this->$finder_act($_GET['id']);
		echo $html;
	}
	
	//获取门店信息
	public function getStore(){
		$seller_id = $_POST['seller_id'];
		$store = $this->app->model('stores')->getList('*', array('seller_id'=>$seller_id));

		if(empty($store)){
			kernel::single('base_code')->_print('10000', '', '该商户下没有门店');
		}
		$list = '';
		foreach($store as $v){
			$list .= '<label><input type="radio" name="num" value="' . $v['num'] . '">' . $v['name'] . '</label>&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		kernel::single('base_code')->_print('0000', $list, '返回成功');
	}
}