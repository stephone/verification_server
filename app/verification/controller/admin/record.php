<?php
/**
 * 核销记录管理
 * @author Administrator
 *
 */
class verification_ctl_admin_record extends desktop_controller{
	
	
	public function index(){
		$this->finder ( 'verification_mdl_record', array (
				'title' => '核销记录',
				'use_buildin_set_tag' => true,
				'use_buildin_filter' => true,
				'use_buildin_tagedit' => true,
				'orderBy'=>'record_id DESC',
				'use_buildin_filter'=>true
		) );
	}
}