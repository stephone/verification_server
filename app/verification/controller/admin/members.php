<?php
/**
 * 核销商户帐号
 * @author ym.liu
 *
 */
class verification_ctl_admin_members extends desktop_controller {
	private $pagelimit = 5;

	public function index() {
		$this->finder ( 'verification_mdl_members', array (
				'title' => '帐号管理',
				'use_buildin_set_tag' => true,
				'use_buildin_filter' => true,
				'use_buildin_tagedit' => true,
				'orderBy' => 'member_id desc',
				
		) );
	}
	
	//新增商户类型账户
	public function addMember() {
		if ($_POST) {		
			$members = $this->app->model('members');

			$data = array(
					'username'=>I('post.username', '', 'strip_tags,trim'),
					'password'=>I('post.password'),
					'status'=>I('post.status'),
					'type'=>I('post.type'),
					'create_time'=>time(),
					'seller_id'=>I('post.seller_id'),
					'store_id'=>I('post.store_id')
			);
			$this->begin();

			//数据验证
			$vRes = $members->validate($data, true);

			if(!$vRes['status']){
				$this->end($vRes['status'], $vRes['message']);
			}

			$data['password'] = md5($data['password']);
			$res = $members->save ( $data );
			
			if($res){
				$this->end(true, '添加成功！');
			} else {
				$this->end(false, '添加失败');
			}
		} else {

			//商户页视图
			if(isset($_GET['seller_id'])){
				$seller = $this->app->model('sellers')->getList('seller_name,seller_id', array('seller_id'=>I('get.seller_id')));
				$this->pagedata['seller'] = $seller[0];
			}

			//门店页添加帐号视图
			if(isset($_GET['store_id'])){
				//获取门店信息
				$store = $this->app->model('stores')->getList('store_name,store_id', array('store_id'=>I('get.store_id')));
				$this->pagedata['store']  = $store[0];
			}
			$this->page ( 'admin/members/addMember.html' );
		}
	}
	
	// 修改用户信息
	public function editMember() {
		if($_POST){
			$data = array(
					'member_id'=>I('post.member_id'),
					'last_time'=>time(),
					'username'=>I('post.username', '', 'strip_tags,trim'),
					'status'=>I('post.status'),
			);
			
			$filter = array(
				'member_id'=>I('post.member_id', 0, 'intval'),
			);
			
			//如果修改时，填入了重置密码
			empty($_POST['password']) ? '' : $data['password'] = I('post.password', '', 'md5');

			$members = $this->app->model('members');
			$vRes = $members->validate($data, false);
			if(!$vRes['status']){
				$this->begin();
				$this->end($vRes['status'], $vRes['message']);
			}

			$res = $members->update($data, $filter);

			$this->begin();
			if($res){
				$this->end($res, '修改成功');
			} else {
				$this->end($res, '修改失败');
			}
		} else {
			
			//获取账户信息
			$member = $this->app->model('members')->getList('*', array('member_id'=>$_GET['member_id']));
			$memberList = $member[0];
			
			//获取商户信息。商户名
			if(!empty($memberList['seller_id'])){
				$seller = $this->app->model('sellers')->getList('*', array('seller_id'=>$memberList['seller_id']));
				$this->pagedata['seller'] = $seller[0];
			}
			
			//获取门店信息
			if(!empty($memberList['store_id'])){
				$store = $this->app->model('stores')->getList('*', array('store_id'=>$memberList['store_id']));
				$this->pagedata['store'] = $store[0];
			}
			
			$this->pagedata['member'] = $member[0];
			$this->page('admin/members/addMember.html');
		}
	}

	//通过商户号查看所有帐号
	function detail_members($seller_id){

		if(!$seller_id){
			return false;
		}

		$nPage = $_GET['detail_members'] ? $_GET['detail_members'] : 1;
		$limit = $this->pagelimit*($nPage-1) . ',' . $this->pagelimit;

		$app = app::get('verification');
		$sql = "select m.member_id,m.username,m.status,m.type,m.store_id,s.store_name from sdb_verification_members m left join sdb_verification_stores s on m.store_id = s.store_id where m.seller_id = {$seller_id} limit {$limit}";
		$members = kernel::database()->select($sql);
		$render = $app->render();
		$render->pagedata['members'] = $members;

		//分页相关
		$count = $app->model('members')->count(array('seller_id'=>$seller_id));
		if($_GET['page']) unset($_GET['page']);
		$_GET['page'] = 'detail_members';
		
		kernel::single('verification_ctl_admin_members')->pagination($nPage,$count,$_GET);

		return $render->fetch('admin/sellers/finder/memberdetail.html');
	}

	//通过商户号查看所有帐号
	function detail_storemembers($store_id){

		if(!$store_id){
			return false;
		}

		$nPage = $_GET['detail_storemembers'] ? $_GET['detail_storemembers'] : 1;
		$limit = $this->pagelimit*($nPage-1) . ',' . $this->pagelimit;

		$app = app::get('verification');
		$sql = "select m.member_id,m.username,m.status,m.type,m.store_id,s.store_name from sdb_verification_members m left join sdb_verification_stores s on m.store_id = s.store_id where m.store_id = {$store_id} limit {$limit}";
		$members = kernel::database()->select($sql);
		$render = $app->render();
		$render->pagedata['members'] = $members;

		//分页相关
		$count = $app->model('members')->count(array('store_id'=>$store_id));
		if($_GET['page']) unset($_GET['page']);
		$_GET['page'] = 'detail_storemembers';
		
		kernel::single('verification_ctl_admin_members')->pagination($nPage,$count,$_GET);

		return $render->fetch('admin/sellers/finder/memberdetail.html');
	}

	//分页
	public function pagination($current,$count,$get){ //本控制器公共分页函数
		$app = app::get('verification');
		$render = $app->render();
		$ui = new base_component_ui($this->app);
		//unset($get['singlepage']);
		$link = 'index.php?app=verification&ctl=admin_members&act=ajax_html&id='.$get['id'].'&finder_act='.$get['page'].'&'.$get['page'].'=%d';
		$this->pagedata['pager'] = $ui->pager(array(
				'current'=>$current,
				'total'=>ceil($count/5),
				'link' =>$link,
		));
	}
	
	public function ajax_html()
	{
		$finder_act = $_GET['finder_act'];
		$html = $this->$finder_act($_GET['id']);
		echo $html;
	}
}