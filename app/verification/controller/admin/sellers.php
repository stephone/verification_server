<?php
/**
 * 商户控制器
 * @author ym.liu
 *
 */
class verification_ctl_admin_sellers extends desktop_controller {
	public $pagelimit = 5;
	
	// 商户后台首页
	public function index() {
		$this->finder ( 'verification_mdl_sellers', array (
				'title' => '商户列表',
				'actions' => array (
						array (
								'label' => '添加商户',
								'href' => 'index.php?app=verification&ctl=admin_sellers&act=add',
								'target' => 'dialog::{frameable:true, title:\'' . '添加商户' . '\',width:500,height:230}' 
						) 
				),
				'orderBy' => 'sort desc',
				'allow_detail_popup' => true,
				'use_buildin_import' => true,
				'use_buildin_export' => true,
				'use_buildin_filter'=>true
				
		) );
	}
	
	// 添加商户
	public function add() {
		if ($_POST) {
			$data = array (
					'seller_id' => I ( 'post.seller_id' ),
					'seller_name' => I ( 'post.seller_name' ),
					'shortname' => I('post.shortname'),
					'sort'=>I('post.sort'),
					'create_time'=>time()
			);

			$this->begin ();
			$res = kernel::single('verification_mdl_sellers')->validate($data);
			if(!$res['status']){
				$this->end(false, $res['message']);
			}

			//检查是否已经存在商户
			$count = $this->app->model('sellers')->count(array('seller_id'=>$data['seller_id']));

			if($count > 0){
				$this->end(false, "该商户号已存在");
			}

			
			$res = $this->app->model ( 'sellers' )->save ( $data );
			
			if ($res) {
				$this->end ( $res, '添加成功' );
			} else {
				$this->end ( $res, '添加失败' );
			}
		} else {
			$this->page ( 'admin/sellers/add.html' );
		}
	}
	
	// 编辑商户
	public function edit() {
		if ($_POST) {
			$data = array (
					'seller_name' => I ( 'post.seller_name' ),
					'seller_id'=>I('post.seller_id'),
					'shortname' => I('post.shortname'),
					'sort'=>I('post.sort'),
					'last_time'=>time()
			);
			$this->begin ();
			$res = kernel::single('verification_mdl_sellers')->validate($data);
			if(!$res['status']){
				$this->end(false, $res['message']);
			}

			$res = $this->app->model ( 'sellers' )->update ( $data, array('seller_id'=>I('post.seller_id')) );
			
			if ($res) {
				$this->end ( $res, '修改商户成功' );
			} else {
				$this->end ( $res, '修改商户失败' );
			}
		} else {
			$data = $this->app->model ( 'sellers' )->getList ( '*', array (
					'seller_id' => I ( 'get.seller_id' ) 
			) );
			$this->pagedata ['info'] = $data [0];
			$this->page ( 'admin/sellers/add.html' );
		}
	}
}