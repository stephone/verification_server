<?php
/**
 * 商户/门店信息表
 * @author ym.liu
 */
$db ['stores'] = array (
	'columns' => array (
		'store_id' => array (
			'required' => true,
			'type' => 'number',
			'pkey' => true,
			'extra' => 'auto_increment',
			'label' => 'ID号'
			),   
		'store_num' => array (
			'type' => 'char(8)',
			'label' => '门店编号/终端号',
			'required' => true,
			'in_list' => true, 
			'required'=>true,
			'default_in_list' => true,
			'searchtype'=>'has',
			),
		'seller_id' => array (
				'required' => true,
				'type' => 'char(15)',
				'label' => '商户ID号',
				'in_list' => true,
				'default_in_list' => true,
				'is_title'=>true,
				'searchtype'=>'has',
		),
		'store_name' => array (
			'type' => 'varchar(50)',
			'label' => '门店名称',
			'required' => true,
			'in_list' => true,
			'default_in_list' => true,
			'searchtype'=>'has',
			),
		'address'=>array(
			'type'=>'varchar(140)',
			'label'=>'门店地址'
			),
		'tel'=>array(
			'type'=>'varchar(20)',
			'label'=>'门店电话'
			),
		'create_time'=>array(
			'type'=>time,
			'filtertype' => 'time',
			'label'=>'添加时间',
			'filterdefault' => true,
			),
		'last_time'=>array(
			'type'=>time,
			'label'=>'最后更新时间'
			),
		'images' =>
		array (
			'type' => 'varchar(150)',
			'label' => '门店图品',
			'width' => 110,
			'hidden' => true,
			'editable' => false,
			'in_list' => false,
			),
		'province' => 
		array (
			'label' => '省',
			'type' => 'varchar(10)',
			),
		'city'=>array(
			'label'=>'市',
			'type'=>'varchar(20)'
			),
		'area'=>array(
			'label'=>'区',
			'type'=>'varchar(20)'
			),
		'longitude' => 
		array (
			'type' => 'varchar(50)',
			'label' => '经度',
			'width' => 110,
			'editable' => false,
			'in_list' => true,
			),
		'latitude' => 
		array (
			'type' => 'varchar(50)',
			'label' => '纬度',
			'width' => 110,
			'editable' => false,
			'in_list' => true,
			),
		'sort'=>array(
			'type'=>'mediumint(5)',
			'default'=>1,
			'label'=>'排序'
			)
		),
'index'=>array(
	'index_store_num'=>array(
		'columns'=>array('store_num'),
		'prefix'=>'UNIQUE'
		),
	'seller_id'=>array(
		'columns'=>array('seller_id'),
		)
	),
'engine' => 'innodb'
);
