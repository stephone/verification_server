<?php
/**
 * 核销记录表
 * @author ym.liu
 */
$db ['record'] = array ( 
		'columns' => array (
				'record_id' => array (
						'type' => number,
						'pkey' => true,
						'extra' => 'auto_increment' 
				), 
				'member_id' => array (
						'required' => true,
						'type' => 'number',
						'extra' => 'auto_increment',
						'label' => '用户ID号',
						'in_list' => true,
						'default_in_list' => true,
				),
				'username' => array (
						'type' => 'varchar(30)',
						'required' => true,
						'label' => '用户名',
						'in_list' => true,
						'default_in_list' => true,
						'searchtype'=>'has',
				),
				'trading_time' => array (
						'type' => 'time',
						'required' => true,
						'label' => '核销时间',
						'default' => 0,
						'in_list' => true,
						'default_in_list' => true,
						'filtertype' => 'time',
           			 	'filterdefault' => true,
				),
				'code' => array (
						'type' => 'varchar(30)',
						'required' => true,
						'label' => '核销码',
						'in_list' => true,
						'default_in_list' => true,
						'filtertype' => 'normal',
						'searchtype'=>'has',
				),
				'sum'=>array(
						'type'=>'tinyint',
						'required'=>true,
						'default'=>1,
						'label'=>'数量'
				),
				'order_id' => array (
						'type' => 'varchar(32)',
						'required' => true,
						'label' => '订单号',
						'width' => 110,
						'in_list' => true,
						'default_in_list' => true,
					'searchtype'=>'has',
				),
				'payment' => array (
						'type' => 'money',
						'required' => true,
						'default' => '0.000',
						'label' => '核销金额',
						'filtertype' => 'yes',
						'editable' => false,
						'in_list' => true,
						'default_in_list' => true 
				),
				'seller_id' => array (
						'required' => true,
						'type' => 'char(15)',
						'label' => '商户ID号',
						'in_list' => true,
						'default_in_list' => true,
						'is_title'=>true,
						'searchtype'=>'has',
				),
				'seller_name' => array (
						'type' => 'varchar(100)',
						'required' => true,
						'label' => '商户名称',
						'in_list' => true,
						'default_in_list' => true,
					'searchtype'=>'has',
				),
				'store_name' => array (
						'type' => 'varchar(50)',
						'required' => true,
						'label' => '门店名称',
						'in_list' => true,
						'default_in_list' => true,
						'searchtype'=>'has',
				),
				'store_num' => array (
						'required' => true,
						'type' => 'char(8)',
						'label' => '门店编号/终端号',
						'in_list' => true,
						'default_in_list' => true,
					'searchtype'=>'has',
				),
				'member_id' => array (
						'required' => true,
						'type' => 'number',
						'label' => '用户ID号',
				),
				'username' => array (
						'type' => 'varchar(30)',
						'required' => true,
						'label' => '用户名',
						'in_list' => true,
						'default_in_list' => true,
						'searchtype'=>'has',
				),
				//接口获取的商城数据
				'name' =>
		        array (
		            'type' => 'varchar(150)',
		            'width' => 110,
		            'label' => '商品名称',
		            'searchtype' => 'has',
		            'in_list' => true,
		            'default_in_list' => true,
		        ),
		         'image_url'=>array(
			      'label'=>'图片地址',
			      'type'=>'varchar(250)',
			      'width'=>300,
			      'in_list'=>false,
			    ),
				'start_time'=>array(
		            'type'=>'time',
		            'width' => 110,
		            'label' => '有效开始时间',
		            'editable' => false,
		            'in_list' => true,
		            'default_in_list' => true,
		            'filtertype' => 'time',
            'filterdefault' => true,
		        ),
		        'end_time'=>array(
		            'type'=>'time',
		            'width' => 110,
		            'label' => '有效结束时间',
		            'editable' => false,
		            'in_list' => true,
		            'default_in_list' => true,
		            'filtertype' => 'time',
            'filterdefault' => true,
		        ),
				//接口获取的商城数据
				'sync'=>array(
					'type'=>array(
						'no'=>'未同步',
						'ok'=>'已同步'
					),
					'default'=>'no',
					'label'=>'是否同步商城',
					'in_list'=>true,
					'default_in_list'=>true,
					'filtertype' => 'yes',
            		'filterdefault' => true,
				), 
				'status'=>array(
					'type'=>array(
						'notice'=>'核销确认通知',		//需要发送核销通知才完成核销
						'complete'=>'已核销'
					),
					'default'=>'notice',
					'in_list'=>true,
					'default_in_list'=>true,
					'label'=>'核销状态',
					'filterdefault' => true,
					'filtertype' => 'yes' 
				) 
		),
		'index'=>array(
				'index_code'=>array(
						'columns'=>array('code')
				) 
		),
		'comment' => app::get ( 'verification' )->_ ( '核销记录表' ),
		'engine' => 'innodb'
);
