<?php
/**
 * 商户/门店信息表
 * @author ym.liu
 */
$db ['sellers'] = array (
		'columns' => array (
				'seller_id' => array (
						'required' => true,
						'type' => 'char(15)',
						'pkey' => true,
						'label' => '商户ID号',
						'in_list' => true,
						'default_in_list' => true,
						'is_title'=>true,
						'searchtype'=>'has',
						'filtertype' => 'normal'
				), 
				'seller_name' => array (
						'type' => 'varchar(100)',
						'label' => '商户名称',
						'required' => true,
						'in_list' => true,
						'default_in_list' => true, 
						'searchtype'=>'has',
						'filtertype' => 'normal'
				), 
				'shortname'=>array(
						'type'=>'varchar(40)',
						'label'=>'商户简称',
						'in_list'=>true,
						'default_in_list'=>true,
						'searchtype'=>'has',
						'filtertype' => 'normal'
					),
				'last_time'=>array(
						'type'=>time,
						'commint'=>'最后更新时间'
				),
				'sort'=>array(
					'type'=>'mediumint(5)',
					'default'=>1,
					'label'=>'排序'
					) 
		), 
		'comment' => app::get ( 'verification' )->_ ( '商户信息表' ),
		'engine' => 'innodb' 
);
