<?php
/**
 * 核销帐号表
 * @author ym.liu
 */
$db ['members'] = array ( 
		'columns' => array (
				'member_id' => array (
						'required' => true,
						'type' => 'number',
						'pkey' => true,
						'extra' => 'auto_increment',
						'label' => '用户ID号'
				), 
				'username' => array (
						'type' => 'varchar(30)',
						'required' => true,
						'label' => '用户名',
						'in_list' => true,
						'default_in_list' => true,
						'searchtype'=>'has',
				),
				'password' => array (
						'type' => 'char(32)',
						'required' => true,
						'label' => '密码' 
				),
				'status' => array (
						'type' => array('true'=>'启用','false'=>'禁用'),
						'default' => 'true',
						'label'=>'是否启用',
						'required' => true,
						'in_list'=>true,
						'default_in_list'=>true,
						'filtertype' => 'yes',
            		'filterdefault' => true,
				),
				'type'=>array(
						'type'=>array('seller'=>'商户帐号','store'=>'门店帐号'),
						'default'=>'store',
						'label'=>'账户类型',
						'required'=>true,
						'in_list' => true,
						'default_in_list' => true,
						'searchtype'=>'has',
				),
				'seller_id' => array (
						'required' => true,
						'type' => 'char(15)',
						'label' => '商户ID号',
						'in_list' => true,
						'default_in_list' => true,
						'is_title'=>true,
						'searchtype'=>'has',
				),
				'store_id'=>array(
						'type'=>'number',
						'label'=>'门店ID号'
					),
				'create_time'=>array(
						'type'=>time,
						'label'=>'帐号创建时间',
						'filtertype' => 'time',
            			'filterdefault' => true,
				),
				'last_time'=>array(
						'type'=>time,
						'commint'=>'最后更新时间'
				)
		), 
		'index'=>array(
				'index_username'=>array(
						'columns'=>array('username'),
						'prefix'=>'UNIQUE'
				),
		), 
		'engine' => 'innodb'
);
