<?php

$db['order_revoke'] = array(
    'columns' =>array (
        'revoke_id' => array(
            'type'=> 'varchar(32)',
            'pkey' => true,
            'required' => true,
            'label' => app::get('qrcode')->_('商城撤销单号')
        ),
         'order_id' =>array (
            'type' => 'varchar(32)',
            'required' => true,
            'label' => app::get('qrcode')->_('订单号'),
        ),
        'sdorder_id' =>array (
            'type' => 'varchar(100)',
            'required' => true,
            'label' => app::get('life')->_('支付订单号'),
		
	),
        
        'tread_no' => array(
            'type' => 'varchar(20)',
            'required' => true,
            'label' => app::get('qrcode')->_('交易流水号')
        ),
        'revoke_status' =>
            array (
                'type' => array(
                    '0'=> app::get('qrcode')->_('未撤销'),
                    '1'=> app::get('qrcode')->_('已撤销'),
                ),
                'default'=>'0',
                'label' => app::get('qrcode')->_('撤销状态'),
        ),
        'revoke_desc' => array(
            'type' => 'varchar(1000)',
            'label' => app::get('qrcode')->_('撤销原因')
        ),
        'money' => array(
            'type' => 'money',
            'required' => 'true',
            'label' => app::get('qrcode')->_('订单金额')
        ),
       
        'create_time' => array(
            'type' => 'time',
            'requirede' => 'true',
            'label' => app::get('qrcode')->_('创建时间')
        ),
        'revoke_no' => array(
            'type'=> 'varchar(32)',
            'label' => app::get('qrcode')->_('支付撤销单号')
        ),
        'revoke_amount' => array(
            'type' => 'money',
            'label' => app::get('qrcode')->_('撤销金额')
        ),
        'revoke_time' => array(
            'type' => 'time',
            'label' => app::get('qrcode')->_('撤销时间')
        ),
    ),
    'index' => array(
        'ix_order_id' => array(
            'columns' => array(
                0 => 'order_id'
            )
        ),
        'ix_sdorder_id' => array(
            'columns' => array(
                0 => 'sdorder_id'
            )
        ),
 
        'ix_revoke_time' => array(
            'columns' => array(
                0 => 'revoke_time'
            )
        )
    ),
    'comment' => app::get('qrcode')->_('POS扫描支付撤销'),
    'engine' => 'innodb'
);


