<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */
 
 
$db['refunds']=array (
  'columns' => 
  array (
    'refund_id' => 
    array (
      'type' => 'varchar(20)',
      'required' => true,
      'default' => '',
      'pkey' => true,
      'label' => app::get('ectools')->_('商城退款单号'),
      'width' => 110,
      'editable' => false,
      'searchtype' => 'has',
      'in_list' => true,
      'default_in_list' => true,
      'is_title' => true,
    ),
    'money' => 
    array (
      'type' => 'money',
      'default' => '0',
      'required' => true,
      'editable' => false,
    ),
    'back_amount' =>
      array (
          'type' => 'money',
          'default' => '0',
          'required' => true,
          'editable' => false,
          'label' =>app::get('ectools')->_('订单返现金额'),
          'in_list' => true,
          'default_in_list' => true,
    ),             
      'real_back_amount' =>
      array (
          'type' => 'money',
          'default' => '0',
          'required' => true,
          'editable' => false,
          'label' =>app::get('ectools')->_('退款扣除金额'),
          'in_list' => true,
          'default_in_list' => true,
      ),
	'cur_money' => 
    array (
      'type' => 'money',
      'default' => '0',
      'required' => true,
	  'label' =>app::get('ectools')->_('退款金额'),
	  'width' => 75,
      'searchtype' => 'nequal',
      'editable' => false,
	  'in_list' => true,
	  'default_in_list' => true,
    ),
     'member_id' => 
    array (
      'type' => 'varchar(100)',
      'label' => app::get('ectools')->_('会员用户名'),
      'width' => 75,
      'editable' => false,
      'in_list' => true,
    ),
    'account' => 
    array (
      'type' => 'varchar(50)',
      'label' => app::get('ectools')->_('收款账号'),
      'width' => 110,
      'searchtype' => 'tequal',
      'editable' => false,
      'in_list' => true,
    'default_in_list' => true,
    ),
    'bank' => 
    array (
      'type' => 'varchar(50)',
      'label' => app::get('ectools')->_('收款银行'),
      'width' => 110,
      'editable' => false,
      'in_list' => true,
    ),
    'pay_account' => 
    array (
      'type' => 'varchar(50)',
      'label' => app::get('ectools')->_('支付账户'),
      'width' => 110,
      'editable' => false,
      'filtertype' => 'normal',
      'in_list' => true,
    ),
    'currency' => 
    array (
      'type' => 'varchar(10)',
      'label' => app::get('ectools')->_('货币'),
      'width' => 75,
      'editable' => false,
      'in_list' => true,
    ),
   
    'paycost' => 
    array (
      'type' => 'money',
      'label' => app::get('ectools')->_('支付网关费用'),
      'width' => 110,
      'editable' => false,
      'in_list' => false,
    ),
    'pay_type' => 
    array (
      'type' => 
      array (
        'online' => app::get('ectools')->_('在线支付'),
        'offline' => app::get('ectools')->_('线下支付'),
        'deposit' => app::get('ectools')->_('预存款支付'),
      ),
      'default' => 'online',
      'required' => true,
      'label' => app::get('ectools')->_('支付类型'),
      'width' => 110,
      'editable' => false,

      'in_list' => true,
    ),
        'status' => 
    array (
      'type' => 
      array (
        'succ' => app::get('ectools')->_('退款成功'),
        'failed' => app::get('ectools')->_('退款失败'),
        'cancel' => app::get('ectools')->_('未支付'),
        'error' => app::get('ectools')->_('处理异常'),
        'invalid' => app::get('ectools')->_('非法参数'),
        'progress' => app::get('ectools')->_('处理中'),
        'timeout' => app::get('ectools')->_('超时'),
        'ready' => app::get('ectools')->_('准备中'),
      ),
      'default' => 'ready',
      'required' => true,
      'label' => app::get('ectools')->_('退款状态'),
      'width' => 75,
      'editable' => false,
      'filtertype' => 'yes',
      'hidden' => true,
      'filterdefault' => true,
      'in_list' => true,
      'default_in_list' => true,
    ),
     'pay_name' => 
    array (
      'type' => 'varchar(100)',
      'width' => 110,
      'editable' => false,
    ),
     'pay_ver' => 
    array (
      'type' => 'varchar(50)',
      'label' => app::get('ectools')->_('支付版本号'),
      'width' => 110,
      'editable' => false,
      'in_list' => true,
    ),
     'op_id' => 
    array (
      'type' => 'number',//'table:users@desktop',
      'label' => app::get('ectools')->_('操作员'),
      'width' => 110,
      'editable' => false,
      'filtertype' => 'normal',
      'in_list' => true,
     'default_in_list' => true,
    ),
    
    
    'refund_bn' =>
    array (
      'type' => 'varchar(32)',
      'required' => false,
      'default' => '',
      'label' => app::get('ectools')->_('商户退款单号'),
      'width' => 140,
      'editable' => false,
      'searchtype' => 'has',
      'filtertype' => 'yes',
      'in_list' => true,
      'default_in_list' => false,
      'is_title' => true,
    ),
    'payment_refund_bn' =>
    array (
      'type' => 'varchar(32)',
      'required' => false,
      'default' => '',
      'label' => app::get('ectools')->_('支付退款单号'),
      'width' => 140,
      'editable' => false,
      'searchtype' => 'has',
      'filtertype' => 'yes',
      'in_list' => true,
      'default_in_list' => true,
      'is_title' => true,
    ),
    'notify_url' => 
    array (
      'type' => 'varchar(255)',
      'label' => app::get('ectools')->_('退款异步通知地址'),
      'width' => 110,
      'editable' => false,
      'in_list' => false,
    ),

    'pay_app_id' => 
    array (
      'type' => 'varchar(100)',
	  'label' => app::get('ectools')->_('支付方式'),
      'required' => true,
      'default' => 0,
      'editable' => false,
	  'in_list' => true,
      'default_in_list' => true,
    ),
   
   
   
    't_begin' => 
    array (
      'type' => 'time',
      'label' => app::get('ectools')->_('创建时间'),
      'width' => 110,
      'editable' => false,
      'filtertype' => 'time',
      'filterdefault' => true,
      'in_list' => true,
    ),
    't_payed' => 
    array (
      'type' => 'time',
      'label' => app::get('ectools')->_('完成时间'),
      'width' => 110,
      'editable' => false,
      'in_list' => true,
        'filtertype' => 'time',
        'filterdefault' => true,
    ),
    't_confirm' => 
    array (
      'type' => 'time',
      'label' => app::get('ectools')->_('审核时间'),
      'width' => 110,
      'editable' => false,
      'in_list' => true,
        'filtertype' => 'time',
        'filterdefault' => true,
    ),

    'memo' => 
    array (
      'type' => 'longtext',
      'editable' => false,
    ),
    'disabled' => 
    array (
      'type' => 'bool',
      'default' => 'false',
      'editable' => false,
    ),
    'trade_no' => 
    array (
      'type' => 'varchar(30)',
      'editable' => false,
    ),
    /*************清算字段*******************/
	/*
    'liqui_status' =>
    array (
      'type' =>
      array (
        0 => app::get('ectools')->_('默认'),
        1 => app::get('ectools')->_('已抽取'),
        2 => app::get('ectools')->_('清算成功'),
        3 => app::get('ectools')->_('清算失败'),
      ),
      'default' => '0',
      'label' => app::get('ectools')->_('清算状态'),
      'editable' => false,
    ),
   'liqui_time' =>
    array (
      'label' => app::get('ectools')->_('清算时间'),
      'type' => 'time',
      'editable' => false,
    ),
	*/
    'verify'=>array(
        'label' => app::get("ectools")->_("审核状态"),
        'type'=>array(
            '0' =>  app::get("ectools")->_("未审核"),
            '1' =>  app::get("ectools")->_("已审核"),
        ),
        'required' => true,
        'default'=>'0',
        'editable' => false,
        'in_list' => true,
        'width' => 110,
        'filtertype' => 'normal',
        'filterdefault' => true,
    ),
    'refund_type'=>array(
        'label' => app::get("ectools")->_("退款对象类型"),
        'type'=>array(
            'order' =>  app::get("ectools")->_("实物类"),
            'life' =>  app::get("ectools")->_("虚拟类"),
            'ecoupon' =>  app::get("ectools")->_("抵用券"),
            'coach' =>  app::get("ectools")->_("票务类"),
			'flight'=> app::get("ectools")->_("机票类"),
            'movie'=> app::get("ectools")->_("电影票类"),
            'qrcode'=> app::get("ectools")->_("扫码下单类"),
			'shipment'=> app::get("ectools")->_("汽车票类"),
        ),
        'required' => true,
        'default'=>'order',
        'editable' => false,
        'in_list' => true,
        'width' => 110,
        'filtertype' => 'normal',
    ),
      'seller_id'=>array(
          'type' => 'table:sellers@seller',
          'required' => true,
          'default' => 0,
          'width' => 75,
          'label' => app::get('ectools')->_('所属商户'),
          'filtertype' => 'yes',
          'in_list' => true,
          'is_title' => true,
          'default_in_list' => true,
      ),
      'seller_bn'=>
      array(
          'type' => 'varchar(50)',
          'required' => true,
          'default' => '',
          'width' => 75,
          'label' => app::get('ectools')->_('商户号'),
          'filtertype' => 'yes',
          'in_list' => true,
          'is_title' => true,
          'default_in_list' => true
      ),
      'refund_way'=>
      array(
          'type' => array(
              'online'=>app::get("ectools")->_("线上退款"),
              'offline'=> app::get("ectools")->_("线下退款"),
          ),
          'width' => 75,
          'label' => app::get('ectools')->_('退款方式'),
          'filtertype' => 'yes',
          'in_list' => true,
          'is_title' => true,
      ),
	  'payment_type' =>
        array (
            'type' => array(
                '00'=> app::get('ectools')->_('杉德宝支付'),
                '01'=> app::get('ectools')->_('生活杉德卡支付'),
                '02'=> app::get('ectools')->_('杉德卡支付'),
                '03' =>  app::get('ectools')->_('信用卡支付'),
                '04' =>  app::get('ectools')->_('储蓄卡支付'),
            ),
            'label' => app::get('ectools')->_('交易类型'),
            'in_list' => true,
            'default_in_list' => true,
            'width' => 75,
            'filterdefault' => true,
            'filtertype' => 'yes',
        ),
        'account_type' =>
        array (
            'type' => array(
                'A01'=> app::get('ectools')->_('绑定支付'),
                'A04'=> app::get('ectools')->_('读卡器支付'),
                'C01'=> app::get('ectools')->_('通用账户'),
                'C02' =>  app::get('ectools')->_('专用账户'),
                'C03' =>  app::get('ectools')->_('生活杉德记名卡'),
                'C04' =>  app::get('ectools')->_('卡密支付'),
                'C05' =>  app::get('ectools')->_('生活杉德不记名卡'),
                'C06' =>  app::get('ectools')->_('商超百货卡'),
                '000' =>  app::get('ectools')->_('网银支付'),
            ),
            'label' => app::get('ectools')->_('账户类型'),
            'in_list' => true,
            'default_in_list' => true,
            'width' => 75,
            'filterdefault' => true,
            'filtertype' => 'yes',
        ),
      'refund_channel' =>
      array (
          'type' => array(
              '00'=> app::get('ectools')->_('杉德宝'),
              '01'=> app::get('ectools')->_('杉德卡'),
          ),
          'label' => app::get('ectools')->_('退款渠道'),
          'in_list' => true,
          'default_in_list' => true,
          'width' => 75,
          'filterdefault' => true,
          'filtertype' => 'yes',
      ),
    /***************************************/
  ),
  'comment' => app::get('ectools')->_('支付记录'),
  'index' => 
  array (
    'ind_disabled' => 
    array (
      'columns' => 
      array (
        0 => 'disabled',
      ),
    ),
  ),
  'engine' => 'innodb',
  'version' => '$Rev: 41103 $',
);