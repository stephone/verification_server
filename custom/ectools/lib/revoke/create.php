<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class ectools_revoke_create{
    public function __construct($app){        
        $this->app = $app;
    }
    
    /**********商城撤销单ID生成************/
    private function gen_revoke_id(){
        $i = rand(0,9999);
        do{
            if(9999==$i){
                $i=0;
            }
            $i++;
            $revoke_id = "R".date('YmdH').str_pad($i,4,'0',STR_PAD_LEFT);
            $filter = array('revoke_id'=>$revoke_id);
            $count = $this->app->model('order_revoke')->count($filter);
        }while($count);
        return $revoke_id;
    }
    /*****************
     * 撤销单生成
     * 
     * *********************/
    public function generate(&$sdf){
        $obj = $this->app->model("order_revoke");
        $revoke_id = $this->gen_revoke_id();
        $sdf = array(
            'revoke_id'  => $revoke_id,
            'order_id' => $sdf['order_id'],
            'sdorder_id' => $sdf['sdorder_id'],
            'tread_no' => $sdf['tread_no'],
            'revoke_desc' => $sdf['revoke_desc'],
            'money' => $sdf['money'],
            'create_time' => time()
        );
        return $obj->save($sdf);
    }
    
}


