<?php
/**
 * ShopEx licence
 *
 * @copyright  Copyright (c) 2005-2010 ShopEx Technologies Inc. (http://www.shopex.cn)
 * @license  http://ecos.shopex.cn/ ShopEx License
 */
/**
* 该类是系统基本的验证类，必须实现 pam_interface_passport 这个接口
*/
class pam_passport_basic implements pam_interface_passport{

	/**
	* 构造方法,初始化配置信息
	*/
    function __construct(){
        kernel::single('base_session')->start();
        $this->init();
    }
    /**
	* 获取配置信息
	* @return array 返回配置信息数组
	*/
    function init(){
        if($ret = app::get('pam')->getConf('passport.'.__CLASS__)){
            return $ret;
        }else{
            $ret = $this->get_setting();
            $ret['passport_id']['value'] = __CLASS__;
            $ret['passport_name']['value'] = $this->get_name();
            $ret['shopadmin_passport_status']['value'] = 'true';
            $ret['site_passport_status']['value'] = 'true';
            $ret['passport_version']['value'] = '1.5';
            app::get('pam')->setConf('passport.'.__CLASS__,$ret);
            return $ret;
        }
    }
	/**
	* 获取认证方式名称
	* @return string 返回名称
	*/
    function get_name(){
        return app::get('pam')->_('用户登录');
    }
	/**
	* 生成认证表单,包括用户名,密码,验证码等input
	* @param object $auth pam_auth 对象
	* @param string $appid app_id
	* @return string 返回HTML页面
	*/
    function get_login_form($auth, $appid, $view, $ext_pagedata=array()){
        $render = app::get('pam')->render();
        $render->pagedata['callback'] = $auth->get_callback_url(__CLASS__);
        if($auth->is_enable_vcode()){
            $render->pagedata['show_varycode'] = 'true';
            $render->pagedata['type'] = $auth->type;
        }
        if(isset($_SESSION['last_error']) && ($auth->type == $_SESSION['type'])){
            $render->pagedata['error_info'] = $_SESSION['last_error'];
            unset($_SESSION['last_error']);
            unset($_SESSION['type']);
        }
        if($ext_pagedata){
            foreach($ext_pagedata as $key => $v){
                $render->pagedata[$key] = $v;
            }
        }
        return $render->fetch($view,$appid);
    }
	/**
	* 认证用户名密码以及验证码等
	* @param object $auth pam_auth对象
	* @param array $usrdata 认证提示信息
	* @return bool|int返回认证成功与否
	*/
    function login($auth,&$usrdata)
    {
	$_POST['uname'] = trim($_POST['uname']);
	/*
        if($auth->is_enable_vcode())
        {
               $key = $auth->appid;

            if(!base_vcode::verify($key,strval($_POST['verifycode'])))
            {
                $usrdata['log_data'] = app::get('pam')->_('验证码不正确！');
                $_SESSION['error'] = app::get('pam')->_('验证码不正确！');
                return false;
            }
        }*/
        if($auth->type!='member')
        {

            $password_string = pam_encrypt::get_encrypted_password($_POST['password'],$auth->type,array('login_name'=>$_POST['uname']));
            if(!$_POST['uname'] || !$password_string || ($_POST['password']!=='0' && !$_POST['password']))
            {
                $usrdata['log_data'] = app::get('pam')->_('验证失败！');
                $_SESSION['error'] = app::get('pam')->_('用户名或密码错误');
                $_SESSION['error_count'][$auth->appid] = $_SESSION['error_count'][$auth->appid]+1;
                return false;
            }
            $rows = app::get('pam')->model('account')->getList('*',array(
            'login_name'=>$_POST['uname'],
            'login_password'=>$password_string,
            'account_type' => $auth->type,
            'disabled' => 'false',
            ),0,1);

            if($rows[0])
            {
                if($_POST['remember'] === "true") setcookie('pam_passport_basic_uname',$_POST['uname'],time()+365*24*3600,'/');
                else setcookie('pam_passport_basic_uname','',0,'/');
                $usrdata['log_data'] = app::get('pam')->_('用户').$_POST['uname'].app::get('pam')->_('验证成功！');
                unset($_SESSION['error_count'][$auth->appid]);

                if(substr($rows[0]['login_password'],0,1) !== 's')
                {
                    $pam_filter = array(
                                'account_id'=>$rows[0]['account_id']
                                );
                    $string_pass = md5($rows[0]['login_password'].$rows[0]['login_name'].$rows[0]['createtime']);
                    $update_data['login_password'] = 's'.substr($string_pass,0,31);
                    app::get('pam')->model('account')->update($update_data,$pam_filter);
                }

                return $rows[0]['account_id'];
            }
            else
            {
                $usrdata['log_data'] = app::get('pam')->_('用户').$_POST['uname'].app::get('pam')->_('验证失败！');
                $_SESSION['error'] = app::get('pam')->_('用户名或密码错误');
                $_SESSION['error_count'][$auth->appid] = $_SESSION['error_count'][$auth->appid]+1;
                return false;
            }
        }

        elseif($auth->type=='member')
        {
            $retData = kernel::single("ums_login")->verify($_POST);
			//$retData = array('responseCode'=>'31X000','userName'=>'13100000000','userId'=>1678,'eMail'=>'','userInfo'=>array('userId'=>1678,'userName'=>'13100000000'));
	    if(!$retData)
            {
                if($retData['retMag'])
                    $_SESSION['error'] = $retData['retMag'];
                else
                    $_SESSION['error'] = app::get('pam')->_('链接超时,请稍后重试!');
                return false;
            }

            if( $account_id = kernel::single("ums_login")->create_member($retData))
            {

				setcookie('member_passport_basic_uname',$_POST['uname'],time()+365*24*3600,'/');

                return $account_id;
            }
            else
            {

//                if($retData['retMag']){
//					if($retData['responseCode'] == "31U016"){
//						base_kvstore::instance('login_count')->fetch("login_count_".$_POST['uname'],$login_name);
//						if(!$login_name){
//							base_kvstore::instance('login_count')->store("login_count_".$_POST['uname'],'2',(3 * 3600));
//						}elseif($login_name > 1){
//							$n = $login_name - 1;
//							base_kvstore::instance('login_count')->store("login_count_".$_POST['uname'],$n,(3 * 3600));
//						}else{
//							$_SESSION['error'] = app::get('pam')->_('用户被锁定,请过3小时后再尝试！');
//							return false;
//						}
//						base_kvstore::instance('login_count')->fetch("login_count_".$_POST['uname'],$login_num);
//						$_SESSION['error'] = $retData['retMag']."您还有".$login_num."次尝试机会！";
//					}else{
//						$_SESSION['error'] = $retData['retMag'];
//					}
//                }else{
//                    $_SESSION['error'] = app::get('pam')->_('链接超时,请稍后重试!');
//				}
//              return false;

                //2014-08-08 update
                if (!$retData['retMag']) {
                    $_SESSION['error'] = app::get('pam')->_('链接超时,请稍后重试!');
                    return false;
                }

                if ($retData['responseCode'] !== "31U016") {
                    $_SESSION['error'] = $retData['retMag'];
                    return false;
                }

                $login_num = (int)$retData['lastLoginChance'];
                if ($login_num > 0) {
                    $_SESSION['error'] = $retData['retMag'] . "您还有" . $login_num . "次尝试机会！";
                } else {
                    $_SESSION['error'] = app::get('pam')->_('用户被锁定,请过3小时后再尝试！');
                }

                return false;
            }

        }
    }
    /**
    * 退出相关操作
    * @param object $autn pam_auth对象
    * @param string $backurl 跳转地址
    */
    function loginout($auth,$backurl="index.php"){
        unset($_SESSION['account'][$auth->type]);
        unset($_SESSION['last_error']);
        #Header('Location: '.$backurl);
    }

    function get_data(){
    }

    function get_id(){
    }

    function get_expired(){
    }

    /**
	* 得到配置信息
	* @return  array 配置信息数组
	*/
    function get_config(){
        $ret = app::get('pam')->getConf('passport.'.__CLASS__);
        if($ret && isset($ret['shopadmin_passport_status']['value']) && isset($ret['site_passport_status']['value'])){
            return $ret;
        }else{
            $ret = $this->get_setting();
            $ret['passport_id']['value'] = __CLASS__;
            $ret['passport_name']['value'] = $this->get_name();
            $ret['shopadmin_passport_status']['value'] = 'true';
            $ret['site_passport_status']['value'] = 'true';
            $ret['passport_version']['value'] = '1.5';
            app::get('pam')->setConf('passport.'.__CLASS__,$ret);
            return $ret;
        }
    }
    /**
	* 设置配置信息
	* @param array $config 配置信息数组
	* @return  bool 配置信息设置成功与否
	*/
    function set_config(&$config){
        $save = app::get('pam')->getConf('passport.'.__CLASS__);
        if(count($config))
            foreach($config as $key=>$value){
                if(!in_array($key,array_keys($save))) continue;
                $save[$key]['value'] = $value;
            }
            $save['shopadmin_passport_status']['value'] = 'true';

        return app::get('pam')->setConf('passport.'.__CLASS__,$save);

    }
   /**
	* 获取finder上编辑时显示的表单信息
	* @return array 配置信息需要填入的项
	*/
    function get_setting(){
        return array(
            'passport_id'=>array('label'=>app::get('pam')->_('通行证id'),'type'=>'text','editable'=>false),
            'passport_name'=>array('label'=>app::get('pam')->_('通行证'),'type'=>'text','editable'=>false),
            'shopadmin_passport_status'=>array('label'=>app::get('pam')->_('后台开启'),'type'=>'bool','editable'=>false),
            'site_passport_status'=>array('label'=>app::get('pam')->_('前台开启'),'type'=>'bool'),
            'passport_version'=>array('label'=>app::get('pam')->_('版本'),'type'=>'text','editable'=>false),
        );
    }




}
